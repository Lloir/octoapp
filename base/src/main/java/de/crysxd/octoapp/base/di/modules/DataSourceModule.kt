package de.crysxd.octoapp.base.di.modules

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.TypeAdapter
import com.google.gson.TypeAdapterFactory
import com.google.gson.reflect.TypeToken
import dagger.Module
import dagger.Provides
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.models.GcodeHistoryItem
import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.data.source.DataSource
import de.crysxd.octoapp.base.data.source.LocalGcodeFileDataSource
import de.crysxd.octoapp.base.data.source.LocalGcodeHistoryDataSource
import de.crysxd.octoapp.base.data.source.LocalMediaFileDataSource
import de.crysxd.octoapp.base.data.source.LocalOctoPrintInstanceInformationSource
import de.crysxd.octoapp.base.data.source.LocalPinnedMenuItemsDataSource
import de.crysxd.octoapp.base.data.source.RemoteGcodeFileDataSource
import de.crysxd.octoapp.base.data.source.RemoteMediaFileDataSource
import de.crysxd.octoapp.base.data.source.RemoteTutorialsDataSource
import de.crysxd.octoapp.base.data.source.WidgetPreferencesDataSource
import de.crysxd.octoapp.base.di.BaseScope
import de.crysxd.octoapp.base.logging.SensitiveDataMask
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.octoprint.json.HttpUrlAdapter
import de.crysxd.octoapp.octoprint.json.PluginSettingsDeserializer
import de.crysxd.octoapp.octoprint.json.SafeDoubleAdapter
import de.crysxd.octoapp.octoprint.json.SafeFloatAdapter
import de.crysxd.octoapp.octoprint.json.SafeIntAdapter
import de.crysxd.octoapp.octoprint.json.SafeLongAdapter
import de.crysxd.octoapp.octoprint.models.settings.Settings
import okhttp3.Cache
import okhttp3.HttpUrl

@Module
class DataSourceModule {

    @Suppress("UNCHECKED_CAST")
    private fun createGson() = GsonBuilder()
        .registerTypeAdapter(Settings.PluginSettingsGroup::class.java, PluginSettingsDeserializer())
        .registerTypeAdapter(HttpUrl::class.java, HttpUrlAdapter())
        .registerTypeAdapterFactory(object : TypeAdapterFactory {
            override fun <T : Any?> create(gson: Gson?, type: TypeToken<T>?): TypeAdapter<T>? = when (type?.rawType) {
                java.lang.Float::class.java -> SafeFloatAdapter()
                java.lang.Double::class.java -> SafeDoubleAdapter()
                java.lang.Long::class.java -> SafeLongAdapter()
                java.lang.Integer::class.java -> SafeIntAdapter()
                else -> null
            } as TypeAdapter<T>?
        })
        .create()

    @Provides
    fun provideOctoPrintInstanceInformationDataSource(
        sharedPreferences: SharedPreferences,
        sensitiveDataMask: SensitiveDataMask,
    ): DataSource<List<OctoPrintInstanceInformationV3>> = LocalOctoPrintInstanceInformationSource(
        sharedPreferences,
        createGson(),
        sensitiveDataMask
    )

    @Provides
    fun provideGcodeHistoryDataSource(sharedPreferences: SharedPreferences): DataSource<List<GcodeHistoryItem>> =
        LocalGcodeHistoryDataSource(sharedPreferences, Gson())

    @Provides
    fun provideLocalPinnedMenuItemsDataSource(
        sharedPreferences: SharedPreferences,
        octoPrintRepository: OctoPrintRepository,
    ): LocalPinnedMenuItemsDataSource = LocalPinnedMenuItemsDataSource(
        sharedPreferences = sharedPreferences,
        octoPrintRepository = octoPrintRepository
    )

    @Provides
    @BaseScope
    fun provideLocalGcodeFileDataSource(
        context: Context,
        octoPreferences: OctoPreferences,
    ) = LocalGcodeFileDataSource(
        context = context,
        gson = Gson(),
        octoPreferences = octoPreferences
    )

    @Provides
    @BaseScope
    fun provideRemoteGcodeFileDataSource(
        octoPrintProvider: OctoPrintProvider,
        local: LocalGcodeFileDataSource,
        context: Context,
    ) = RemoteGcodeFileDataSource(octoPrintProvider, local, context)

    @Provides
    @BaseScope
    fun provideRemoteTutorialsDataSource(
        context: Context,
        httpCache: Cache,
    ) = RemoteTutorialsDataSource(
        context = context,
        httpCache = httpCache,
    )

    @Provides
    @BaseScope
    fun provideWidgetOrderDataSource(
        sharedPreferences: SharedPreferences,
    ) = WidgetPreferencesDataSource(
        sharedPreferences = sharedPreferences,
        gson = createGson()
    )

    @Provides
    @BaseScope
    fun provideLocalMediaFileDataSource(
        context: Context,
        octoPreferences: OctoPreferences,
    ) = LocalMediaFileDataSource(
        context = context,
        octoPreferences = octoPreferences,
    )

    @Provides
    @BaseScope
    fun provideRemoteMediaFileDataSource(
        octoPrintProvider: OctoPrintProvider,
        octoPrintRepository: OctoPrintRepository,
    ) = RemoteMediaFileDataSource(
        octoPrintRepository = octoPrintRepository,
        octoPrintProvider = octoPrintProvider,
    )
}