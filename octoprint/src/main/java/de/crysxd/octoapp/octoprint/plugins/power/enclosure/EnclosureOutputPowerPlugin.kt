package de.crysxd.octoapp.octoprint.plugins.power.enclosure

import de.crysxd.octoapp.octoprint.models.settings.Settings
import de.crysxd.octoapp.octoprint.plugins.power.PowerPlugin

class EnclosureOutputPowerPlugin(
    private val api: EnclosureOutputApi
) : PowerPlugin<EnclosureOutputPowerDevice> {

    internal suspend fun turnOn(device: EnclosureOutputPowerDevice) {
        api.sendCommand(device.indexId, EnclosureOutputCommand.TurnGpioOn)
    }

    internal suspend fun turnOff(device: EnclosureOutputPowerDevice) {
        api.sendCommand(device.indexId, EnclosureOutputCommand.TurnGpioOff)
    }

    internal suspend fun isOn(device: EnclosureOutputPowerDevice) =
        api.getGpioState(device.indexId).currentValue

    override fun getDevices(settings: Settings) =
        settings.plugins.values.mapNotNull {
            it as? Settings.EnclosureSettings
        }.firstOrNull()?.outputs?.filter {
            // We only support regular IO, no PWM etc
            it.type == "regular"
        }?.map {
            it.copy(plugin = this)
        } ?: emptyList()
}
