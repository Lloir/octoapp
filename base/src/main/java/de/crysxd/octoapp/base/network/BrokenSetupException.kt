package de.crysxd.octoapp.base.network

import android.net.Uri
import de.crysxd.octoapp.octoprint.exceptions.OctoPrintException
import java.io.IOException

class BrokenSetupException(
    val original: OctoPrintException,
    val userMessage: String,
    val navigationAction: Uri?,
) : IOException("Broken setup: ${original.message}", original)