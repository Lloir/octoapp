package de.crysxd.octoapp.octoprint.plugins.power.mystrom

sealed class MyStromCommand(val command: String) {
    object EnableRelay : MyStromCommand("enableRelais")
    object DisableRelay : MyStromCommand("disableRelais")
}

