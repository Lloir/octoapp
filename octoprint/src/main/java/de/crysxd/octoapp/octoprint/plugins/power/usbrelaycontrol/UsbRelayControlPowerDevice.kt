package de.crysxd.octoapp.octoprint.plugins.power.usbrelaycontrol

import com.google.gson.annotations.SerializedName
import de.crysxd.octoapp.octoprint.plugins.power.PowerDevice

data class UsbRelayControlPowerDevice(
    private val plugin: UsbRelayControlPowerPlugin? = null,
    @SerializedName("name") override val displayName: String,
    val index: Int
) : PowerDevice() {
    override val id = index.toString()
    override val pluginId = "usbrelaycontrol"
    override val pluginDisplayName = "USB Relay Control"
    override val capabilities
        get() = listOf(Capability.ControlPrinterPower, Capability.Illuminate)

    override suspend fun turnOn() = plugin?.turnOn(this) ?: throw IllegalStateException("Acquire this class from UsbRelayControlPowerPlugin")
    override suspend fun turnOff() = plugin?.turnOff(this) ?: throw IllegalStateException("Acquire this class from UsbRelayControlPowerPlugin")
    override suspend fun isOn(): Boolean = plugin?.isOn(this) ?: throw IllegalStateException("Acquire this class from UsbRelayControlPowerPlugin")
}
