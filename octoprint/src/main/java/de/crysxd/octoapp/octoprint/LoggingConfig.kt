package de.crysxd.octoapp.octoprint

enum class LoggingConfig {
    Production,
    Verbose,
    Debug,
}