package de.crysxd.octoapp.octoprint.plugins.power

import de.crysxd.octoapp.octoprint.models.settings.Settings
import de.crysxd.octoapp.octoprint.plugins.power.enclosure.EnclosureOutputApi
import de.crysxd.octoapp.octoprint.plugins.power.enclosure.EnclosureOutputPowerPlugin
import de.crysxd.octoapp.octoprint.plugins.power.gpiocontrol.GpioControlPowerPlugin
import de.crysxd.octoapp.octoprint.plugins.power.gpiocontrol.GpioCoontrolApi
import de.crysxd.octoapp.octoprint.plugins.power.mystrom.MyStromApi
import de.crysxd.octoapp.octoprint.plugins.power.mystrom.MyStromPowerPlugin
import de.crysxd.octoapp.octoprint.plugins.power.ocotrelay.OctoRelayApi
import de.crysxd.octoapp.octoprint.plugins.power.ocotrelay.OctoRelayPowerPlugin
import de.crysxd.octoapp.octoprint.plugins.power.octocam.OctoCamApi
import de.crysxd.octoapp.octoprint.plugins.power.octocam.OctoCamPowerPlugin
import de.crysxd.octoapp.octoprint.plugins.power.octohue.OctoHueApi
import de.crysxd.octoapp.octoprint.plugins.power.octohue.OctoHuePowerPlugin
import de.crysxd.octoapp.octoprint.plugins.power.octolight.OctoLightApi
import de.crysxd.octoapp.octoprint.plugins.power.octolight.OctoLightPowerPlugin
import de.crysxd.octoapp.octoprint.plugins.power.ophom.OphomApi
import de.crysxd.octoapp.octoprint.plugins.power.ophom.OphomPowerPlugin
import de.crysxd.octoapp.octoprint.plugins.power.psucontrol.PsuControlApi
import de.crysxd.octoapp.octoprint.plugins.power.psucontrol.PsuControlPowerPlugin
import de.crysxd.octoapp.octoprint.plugins.power.tasmota.TasmotaApi
import de.crysxd.octoapp.octoprint.plugins.power.tasmota.TasmotaPowerPlugin
import de.crysxd.octoapp.octoprint.plugins.power.tplinkplug.TpLinkSmartPlugApi
import de.crysxd.octoapp.octoprint.plugins.power.tplinkplug.TpLinkSmartPlugPowerPlugin
import de.crysxd.octoapp.octoprint.plugins.power.tradfri.TradfriApi
import de.crysxd.octoapp.octoprint.plugins.power.tradfri.TradfriPowerPlugin
import de.crysxd.octoapp.octoprint.plugins.power.tuya.TuyaApi
import de.crysxd.octoapp.octoprint.plugins.power.tuya.TuyaPowerPlugin
import de.crysxd.octoapp.octoprint.plugins.power.usbrelaycontrol.UsbRelayControlApi
import de.crysxd.octoapp.octoprint.plugins.power.usbrelaycontrol.UsbRelayControlPowerPlugin
import de.crysxd.octoapp.octoprint.plugins.power.wemoswitch.WemoSwitchApi
import de.crysxd.octoapp.octoprint.plugins.power.wemoswitch.WemoSwitchPowerPlugin
import de.crysxd.octoapp.octoprint.plugins.power.wled.WledApi
import de.crysxd.octoapp.octoprint.plugins.power.wled.WledPowerPlugin
import de.crysxd.octoapp.octoprint.plugins.power.ws281x.WS281xApi
import de.crysxd.octoapp.octoprint.plugins.power.ws281x.WS281xPowerPlugin
import retrofit2.Retrofit

class PowerPluginsCollection(
    apiRetrofit: Retrofit,
    rootRetrofit: Retrofit,
) {

    val plugins = listOf(
        PsuControlPowerPlugin(apiRetrofit.create(PsuControlApi::class.java)),
        TradfriPowerPlugin(apiRetrofit.create(TradfriApi::class.java)),
        TpLinkSmartPlugPowerPlugin(apiRetrofit.create(TpLinkSmartPlugApi::class.java)),
        TasmotaPowerPlugin(apiRetrofit.create(TasmotaApi::class.java)),
        TuyaPowerPlugin(apiRetrofit.create(TuyaApi::class.java)),
        WS281xPowerPlugin(apiRetrofit.create(WS281xApi::class.java)),
        GpioControlPowerPlugin(apiRetrofit.create(GpioCoontrolApi::class.java)),
        UsbRelayControlPowerPlugin(apiRetrofit.create(UsbRelayControlApi::class.java)),
        MyStromPowerPlugin(apiRetrofit.create(MyStromApi::class.java)),
        OctoRelayPowerPlugin(apiRetrofit.create(OctoRelayApi::class.java)),
        WledPowerPlugin(apiRetrofit.create(WledApi::class.java)),
        OctoCamPowerPlugin(apiRetrofit.create(OctoCamApi::class.java)),
        OctoLightPowerPlugin(apiRetrofit.create(OctoLightApi::class.java)),
        WemoSwitchPowerPlugin(apiRetrofit.create(WemoSwitchApi::class.java)),
        OphomPowerPlugin(apiRetrofit.create(OphomApi::class.java)),
        OctoHuePowerPlugin(apiRetrofit.create(OctoHueApi::class.java)),
        EnclosureOutputPowerPlugin(rootRetrofit.create(EnclosureOutputApi::class.java)),
    )

    fun getDevices(settings: Settings) = plugins.map {
        it.getDevices(settings)
    }.flatten()
}