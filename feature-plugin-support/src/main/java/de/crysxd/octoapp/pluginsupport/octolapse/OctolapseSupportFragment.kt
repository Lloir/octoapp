package de.crysxd.octoapp.pluginsupport.octolapse

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import de.crysxd.baseui.ext.requireOctoActivity
import de.crysxd.octoapp.pluginsupport.di.injectViewModel
import kotlinx.coroutines.flow.collect
import timber.log.Timber

class OctolapseSupportFragment : Fragment() {

    private val viewModel by injectViewModel<OctolapseSupportViewModel>()
    private val bottomSheetTag = "octolapse-bottom-sheet"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.i("Starting Octolapse support")

        lifecycleScope.launchWhenCreated {
            viewModel.events.collect {
                Timber.i("Octolapse event: $it")
                when (it) {
                    is OctolapseSupportViewModel.Event.OpenBottomSheet -> if (childFragmentManager.findFragmentByTag(bottomSheetTag) == null) {
                        try {
                            OctolapseBottomSheetFragment.create(it.plan).show(childFragmentManager, bottomSheetTag)
                        } catch (e: IllegalStateException) {
                            // State loss, ignore
                            Timber.w(e, "Unable to show dialog")
                        }
                    }

                    is OctolapseSupportViewModel.Event.ShowMessage ->
                        requireOctoActivity().showDialog(it.message)
                }
            }
        }
    }
}