package de.crysxd.octoapp.octoprint.plugins.power.tuya

import com.google.gson.annotations.SerializedName
import de.crysxd.octoapp.octoprint.plugins.power.PowerDevice

data class TuyaPowerDevice(
    @Transient val plugin: TuyaPowerPlugin?,
    @SerializedName("label") override val displayName: String,
) : PowerDevice() {

    // Tuya uses display name as id....
    override val id: String = displayName

    override val capabilities
        get() = listOf(Capability.ControlPrinterPower, Capability.Illuminate)

    @Transient
    override val pluginDisplayName = "Tuya"

    @Transient
    override val pluginId = "tuyasmartplug"

    override suspend fun turnOn() = plugin?.turnOn(this)
        ?: throw IllegalStateException("Acquire this class from TuyaPowerPlugin!")

    override suspend fun turnOff() = plugin?.turnOff(this)
        ?: throw IllegalStateException("Acquire this class from TuyaPowerPlugin!")

    override suspend fun isOn() = plugin?.isOn(this)

}