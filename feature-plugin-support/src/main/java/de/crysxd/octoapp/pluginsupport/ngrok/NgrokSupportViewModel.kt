package de.crysxd.octoapp.pluginsupport.ngrok

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.crysxd.octoapp.base.data.models.hasPlugin
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.ext.shouldRetry
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.usecase.UpdateNgrokTunnelUseCase
import de.crysxd.octoapp.octoprint.isNgrokUrl
import de.crysxd.octoapp.octoprint.models.ConnectionType
import de.crysxd.octoapp.octoprint.models.settings.Settings
import de.crysxd.octoapp.octoprint.models.socket.Message
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.flow.shareIn
import timber.log.Timber

class NgrokSupportViewModel(
    octoPrintProvider: OctoPrintProvider,
    octoPrintRepository: OctoPrintRepository,
    private val updateNgrokTunnelUseCase: UpdateNgrokTunnelUseCase,
) : ViewModel() {

    private val credentialsChanges = octoPrintRepository.instanceInformationFlow()
        .map { instance ->
            instance?.alternativeWebUrl?.takeIf { it.isNgrokUrl() }?.let {
                Timber.i("Instance with ngrok configuration was updated, updating ngrok as well")
                try {
                    updateNgrokTunnelUseCase.execute(UpdateNgrokTunnelUseCase.Params.Tunnel(it.toString()))
                } catch (e: Exception) {
                    Timber.e(e, "Failed to update ngrok config")
                }
            }
        }

    private val connectionEvents = octoPrintProvider.passiveConnectionEventFlow("ngrok-support")
        .onEach { event ->
            if (event.connectionType == ConnectionType.Primary && octoPrintRepository.getActiveInstanceSnapshot().hasPlugin(Settings.Ngrok::class)) {
                Timber.i("Instance with ngrok configuration was connected, updating ngrok")
                try {
                    updateNgrokTunnelUseCase.execute(UpdateNgrokTunnelUseCase.Params.FetchConfig)
                } catch (e: Exception) {
                    Timber.e(e, "Failed to update ngrok config")
                }
            }
        }

    private val messages = octoPrintProvider.passiveCachedMessageFlow("ngrok-support", Message.NgrokPluginMessage::class)
        .filterNotNull()
        .onEach {
            Timber.i("Received new ngrok configuration: $it")
            try {
                updateNgrokTunnelUseCase.execute(UpdateNgrokTunnelUseCase.Params.Tunnel(it.tunnel))
            } catch (e: Exception) {
                Timber.e(e, "Failed to update ngrok config")
            }
        }

    val events = credentialsChanges.combine(connectionEvents) { _, _ -> }.combine(messages) { _, _ -> }.retry {
        delay(1000)
        Timber.e(it)
        it.shouldRetry()
    }.shareIn(viewModelScope, replay = 1, started = SharingStarted.Lazily)
}