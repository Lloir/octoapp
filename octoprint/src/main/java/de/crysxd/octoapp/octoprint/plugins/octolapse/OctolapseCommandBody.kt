package de.crysxd.octoapp.octoprint.plugins.octolapse

import com.google.gson.annotations.SerializedName

class OctolapseCommandBody(
    val cancel: Boolean? = null,
    @SerializedName("preprocessing_job_guid") val jobId: String?
)