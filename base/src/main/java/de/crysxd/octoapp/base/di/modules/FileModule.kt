package de.crysxd.octoapp.base.di.modules

import android.content.Context
import android.net.Uri
import androidx.core.content.FileProvider
import dagger.Module
import dagger.Provides
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.R
import de.crysxd.octoapp.base.di.BaseScope
import de.crysxd.octoapp.base.di.modules.FileModule.PublicFileFactory
import okhttp3.Cache
import java.io.File

@Module
class FileModule {

    @Provides
    fun providePublicFileFactory(context: Context, cacheDir: File) = PublicFileFactory { fileName: String ->
        cacheDir.mkdirs()
        val f = File(cacheDir, fileName)
        f.createNewFile()
        f.deleteOnExit()
        val uri = FileProvider.getUriForFile(context, context.applicationContext.packageName + ".provider", f)
        f to uri
    }

    @Provides
    fun providePublicCacheDir(context: Context) = File(context.externalCacheDir, context.getString(R.string.public_file_dir_name)).also {
        it.mkdirs()
    }

    @Provides
    @BaseScope
    fun provideHttpCache(
        context: Context,
        octoPreferences: OctoPreferences,
    ) = Cache(
        File(context.externalCacheDir, "http-cache").also { it.mkdirs() },
        octoPreferences.httpCacheSize,
    )

    fun interface PublicFileFactory {
        fun createPublicFile(fileName: String): Pair<File, Uri>
    }
}