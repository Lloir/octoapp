package de.crysxd.octoapp.octoprint.json

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter

class SafeIntAdapter : TypeAdapter<Int>() {
    override fun write(out: JsonWriter, value: Int?) {
        out.value(value)
    }

    override fun read(reader: JsonReader): Int? = when (reader.peek()) {
        JsonToken.NUMBER -> reader.nextInt()
        JsonToken.STRING -> reader.nextString().toIntOrNull()
        else -> {
            reader.skipValue()
            null
        }
    }
}