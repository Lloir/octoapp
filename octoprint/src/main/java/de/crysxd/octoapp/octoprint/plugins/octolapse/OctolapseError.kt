package de.crysxd.octoapp.octoprint.plugins.octolapse

data class OctolapseError(
    val name: String?,
    val description: String?,
)