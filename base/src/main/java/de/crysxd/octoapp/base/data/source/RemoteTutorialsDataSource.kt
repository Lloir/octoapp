package de.crysxd.octoapp.base.data.source

import android.content.Context
import android.os.Build
import de.crysxd.octoapp.base.api.TutorialsApi
import de.crysxd.octoapp.base.data.models.YoutubePlaylist
import de.crysxd.octoapp.base.logging.TimberLogger
import de.crysxd.octoapp.octoprint.logging.LoggingInterceptorLogger
import okhttp3.Cache
import okhttp3.CacheControl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.io.File
import java.util.Date
import java.util.logging.Logger
import java.util.regex.Pattern

class RemoteTutorialsDataSource(context: Context, httpCache: Cache) {

    private val versionFilter = Pattern.compile("#octoapp:min:(\\d+)")
    private val versionCode by lazy {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            context.packageManager.getPackageInfo(context.packageName, 0).longVersionCode
        } else {
            @Suppress("DEPRECATION")
            context.packageManager.getPackageInfo(context.packageName, 0).versionCode.toLong()
        }
    }

    private val logger = LoggingInterceptorLogger(TimberLogger(Logger.getLogger("YouTube/HTTP")).logger)
    private val cacheDir = File(context.cacheDir, "youtube-cache")
    private var forceNetworkOnNext = false
    private val client = OkHttpClient.Builder()
        .cache(httpCache)
        .addInterceptor(HttpLoggingInterceptor(logger).also { it.level = HttpLoggingInterceptor.Level.HEADERS })
        .addInterceptor {
            val builder = it.request().newBuilder()
            if (forceNetworkOnNext) {
                forceNetworkOnNext = false
                builder.cacheControl(CacheControl.FORCE_NETWORK)
            }
            it.proceed(builder.build())
        }.build()

    private val api = Retrofit.Builder()
        .client(client)
        .baseUrl("https://octoapp.eu/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(TutorialsApi::class.java)

    suspend fun get(skipCache: Boolean): List<YoutubePlaylist.PlaylistItem> {
        if (skipCache) {
            forceNetworkOnNext = true
        }

        Timber.i("Loading playlist")

        val playlist = api.getPlaylist()
        require(!playlist.items.isNullOrEmpty()) { "Playlist is empty" }

        return playlist.items.sortedByDescending {
            it.contentDetails?.videoPublishedAt ?: Date(0)
        }.filter {
            // Private videos have no publishing date
            it.contentDetails?.videoPublishedAt != null
        }.filter {
            // Filter videos for newer app versions
            val matcher = versionFilter.matcher(it.snippet?.description ?: "")
            if (matcher.find()) {
                val minVersionCode = matcher.group(1)?.toLongOrNull() ?: Long.MAX_VALUE
                versionCode >= minVersionCode
            } else {
                true
            }
        }
    }
}