package de.crysxd.octoapp.connectprinter.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.transition.ChangeBounds
import androidx.transition.Fade
import androidx.transition.TransitionManager
import androidx.transition.TransitionSet
import com.transitionseverywhere.ChangeText
import de.crysxd.baseui.BaseFragment
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.common.NetworkStateViewModel
import de.crysxd.baseui.common.OctoToolbar
import de.crysxd.baseui.di.BaseUiInjector
import de.crysxd.baseui.ext.requireOctoActivity
import de.crysxd.baseui.menu.base.MenuBottomSheetFragment
import de.crysxd.baseui.menu.power.PowerControlsMenu
import de.crysxd.baseui.menu.switchprinter.SwitchOctoPrintMenu
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.open
import de.crysxd.octoapp.connectprinter.R
import de.crysxd.octoapp.connectprinter.databinding.ConnectPrinterFragmentBinding
import de.crysxd.octoapp.connectprinter.di.injectViewModel
import de.crysxd.octoapp.octoprint.plugins.power.PowerDevice
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import timber.log.Timber

class ConnectPrinterFragment : BaseFragment(), PowerControlsMenu.PowerControlsCallback {

    private val networkViewModel: NetworkStateViewModel by injectViewModel(BaseUiInjector.get().viewModelFactory())
    override val viewModel: ConnectPrinterViewModel by injectViewModel()
    private lateinit var binding: ConnectPrinterFragmentBinding
    private var setDelayedStatusJob: Job? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        ConnectPrinterFragmentBinding.inflate(inflater, container, false).also { binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Subscribe to network state
        networkViewModel.networkState.observe(viewLifecycleOwner) {
            binding.noWifiWarning.isVisible = it !is NetworkStateViewModel.NetworkState.WifiConnected
        }

        // Subscribe to view state
        viewModel.uiState.observe(viewLifecycleOwner) { state ->
            Timber.i("$state")

            viewLifecycleOwner.lifecycleScope.launchWhenStarted {
                TransitionManager.beginDelayedTransition(binding.root, TransitionSet().also {
                    it.addTransition(Fade(Fade.OUT))
                    it.addTransition(Fade(Fade.IN).setStartDelay(300))
                    it.addTransition(ChangeBounds().apply {
                        excludeChildren(binding.root, true)
                        addTarget(binding.noWifiWarning)
                    })
                    it.addTransition(ChangeText().apply {
                        changeBehavior = ChangeText.CHANGE_BEHAVIOR_OUT_IN
                    })
                })

                handleUiStateUpdate(state)
            }

            binding.buttonMore1.setOnClickListener { showMenu() }
            binding.buttonMore2.setOnClickListener { showMenu() }
            binding.buttonMore3.setOnClickListener { showMenu() }
            binding.buttonMore4.setOnClickListener { showMenu() }
            binding.buttonMore5.setOnClickListener { showMenu() }
            binding.buttonTroubleShoot.setOnClickListener {
                viewModel.activeInstance?.let {
                    UriLibrary.getFixOctoPrintConnectionUri(baseUrl = it.webUrl, instanceId = it.id).open(requireOctoActivity())
                }
            }
            binding.buttonBeginConnect.setOnClickListener {
                startManualConnection()
            }
        }
    }

    fun startManualConnection() {
        val wasInfoShown = BaseInjector.get().octoPreferences().wasAutoConnectPrinterInfoShown
        if (wasInfoShown) {
            requireOctoActivity().showDialog(
                OctoActivity.Message.DialogMessage(
                    text = { getString(R.string.connect_printer___begin_connection_confirmation_message) },
                    positiveButton = { getString(R.string.connect_printer___begin_connection_cofirmation_positive) },
                    positiveAction = { viewModel.beginConnect() },
                    neutralButton = { getString(R.string.connect_printer___begin_connection_cofirmation_negative) },
                )
            )
        } else {
            MenuBottomSheetFragment.createForMenu(AutoConnectPrinterInfoMenu()).show(childFragmentManager)
        }
    }

    private fun showMenu() {
        MenuBottomSheetFragment().show(childFragmentManager)
    }

    override fun onResume() {
        super.onResume()
        viewModel.uiState.value?.let(::handleUiStateUpdate)
    }

    private fun handleUiStateUpdate(state: ConnectPrinterViewModel.UiState) {
        binding.psuTurnOnControls.isVisible = false
        binding.psuTurnOffControls.isVisible = false
        binding.beginConnectControls.isVisible = false
        binding.octoprintNotAvailableControls.isVisible = false
        binding.octoprintConnectedInfo.isVisible = !listOf(
            ConnectPrinterViewModel.UiState.Initializing,
            ConnectPrinterViewModel.UiState.OctoPrintNotAvailable,
            ConnectPrinterViewModel.UiState.OctoPrintStarting
        ).contains(state)
        binding.noWifiWarning.alpha = if (binding.octoprintConnectedInfo.isVisible) 0f else 1f
        binding.buttonChangeOctoPrint.setOnClickListener {
            MenuBottomSheetFragment.createForMenu(SwitchOctoPrintMenu()).show(childFragmentManager)
        }

        when (state) {
            ConnectPrinterViewModel.UiState.OctoPrintNotAvailable -> {
                binding.octoprintNotAvailableControls.isVisible = true
                binding.octoView.swim()
                showStatus(
                    R.string.connect_printer___octoprint_not_available_title,
                    R.string.connect_printer___octoprint_not_available_detail
                )
            }

            ConnectPrinterViewModel.UiState.OctoPrintStarting -> {
                binding.octoView.swim()
                showStatus(R.string.connect_printer___octoprint_starting_title)
            }

            is ConnectPrinterViewModel.UiState.WaitingForPrinterToComeOnline -> {
                binding.octoView.idle()
                binding.buttonTurnOnPsu.setOnClickListener {
                    MenuBottomSheetFragment.createForMenu(PowerControlsMenu(type = PowerControlsMenu.DeviceType.PrinterPsu, action = PowerControlsMenu.Action.TurnOn))
                        .show(childFragmentManager)
                }
                binding.buttonTurnOffPsu.setOnClickListener {
                    MenuBottomSheetFragment.createForMenu(PowerControlsMenu(type = PowerControlsMenu.DeviceType.PrinterPsu, action = PowerControlsMenu.Action.TurnOff))
                        .show(childFragmentManager)
                }
                binding.psuTurnOnControls.isVisible = state.psuIsOn == false
                binding.psuTurnOffControls.isVisible = state.psuIsOn == true
                binding.buttonTurnOnPsu.text = getString(R.string.connect_printer___action_turn_psu_on)
                binding.buttonTurnOffPsu.text = getString(R.string.connect_printer___action_turn_psu_off)
                showStatus(
                    if (state.psuIsOn == true) {
                        R.string.connect_printer___psu_on_waiting_for_printer_title
                    } else {
                        R.string.connect_printer___waiting_for_printer_title
                    },
                    R.string.connect_printer___waiting_for_printer_detail
                )
            }

            ConnectPrinterViewModel.UiState.PrinterConnecting -> {
                binding.octoView.swim()
                showStatus(R.string.connect_printer___printer_is_connecting_title)
            }

            is ConnectPrinterViewModel.UiState.PrinterOffline -> {
                binding.octoView.idle()
                binding.psuTurnOnControls.isVisible = true
                binding.buttonTurnOnPsu.setOnClickListener {
                    if (state.psuSupported) {
                        MenuBottomSheetFragment.createForMenu(
                            PowerControlsMenu(
                                type = PowerControlsMenu.DeviceType.PrinterPsu,
                                action = PowerControlsMenu.Action.Cycle
                            )
                        )
                            .show(childFragmentManager)
                    } else {
                        viewModel.retryConnectionFromOfflineState()
                    }
                }
                binding.buttonTurnOnPsu.setText(
                    if (state.psuSupported) {
                        R.string.connect_printer___action_cycle_psu
                    } else {
                        R.string.connect_printer___action_retry
                    }
                )
                showStatus(
                    R.string.connect_printer___printer_offline_title,
                    if (state.psuSupported) {
                        R.string.connect_printer___printer_offline_detail_with_psu
                    } else {
                        R.string.connect_printer___printer_offline_detail
                    }
                )
            }

            is ConnectPrinterViewModel.UiState.PrinterPsuCycling -> {
                binding.octoView.swim()
                showStatus(R.string.connect_printer___psu_cycling_title)
            }

            ConnectPrinterViewModel.UiState.WaitingForUser -> {
                binding.octoView.idle()
                binding.beginConnectControls.isVisible = true
                showStatus(
                    R.string.connect_printer___waiting_for_user_title,
                    R.string.connect_printer___waiting_for_user_subtitle
                )
            }

            ConnectPrinterViewModel.UiState.Initializing -> {
                binding.octoView.swim()
                showStatus(R.string.connect_printer___searching_for_octoprint_title)
            }

            ConnectPrinterViewModel.UiState.PrinterConnected -> {
                binding.octoView.idle()
                showStatus(R.string.connect_printer___printer_connected_title, R.string.connect_printer___printer_connected_detail_1)
                showStatusDelayed(R.string.connect_printer___printer_connected_title, R.string.connect_printer___printer_connected_detail_2)
            }

            ConnectPrinterViewModel.UiState.Unknown -> {
                binding.octoView.swim()
                showStatus(
                    R.string.error_general,
                    R.string.try_restrating_the_app_or_octoprint
                )
            }
        }.let { }

        binding.psuUnvailableControls.isVisible = !binding.psuTurnOnControls.isVisible &&
                !binding.psuTurnOffControls.isVisible &&
                !binding.beginConnectControls.isVisible &&
                !binding.octoprintNotAvailableControls.isVisible

    }

    private fun showStatusDelayed(@StringRes state: Int, @StringRes subState: Int? = null) {
        setDelayedStatusJob = viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            delay(10000)
            showStatus(state, subState)
        }
    }

    private fun showStatus(@StringRes state: Int, @StringRes subState: Int? = null) {
        setDelayedStatusJob?.cancel()
        binding.textViewState.text = getString(state)
        binding.textViewSubState.text = subState?.let(this::getString)
    }

    override fun onStart() {
        super.onStart()
        requireOctoActivity().octoToolbar.state = OctoToolbar.State.Connect
        requireOctoActivity().octo.isVisible = false
    }

    override fun onPowerActionCompleted(action: PowerControlsMenu.Action, device: PowerDevice) {
        when (action) {
            PowerControlsMenu.Action.TurnOn -> viewModel.setDeviceOn(true)
            PowerControlsMenu.Action.TurnOff -> viewModel.setDeviceOn(false)
            PowerControlsMenu.Action.Toggle -> viewModel.setDeviceOn(
                (viewModel.uiState.value as? ConnectPrinterViewModel.UiState.WaitingForPrinterToComeOnline)?.psuIsOn?.not() ?: false
            )
            PowerControlsMenu.Action.Cycle -> viewModel.cyclePsu()
            else -> Unit
        }
    }
}