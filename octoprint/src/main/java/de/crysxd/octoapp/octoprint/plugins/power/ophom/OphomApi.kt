package de.crysxd.octoapp.octoprint.plugins.power.ophom

import retrofit2.http.GET

interface OphomApi {

    @GET("plugin/ophom?action=toggle")
    suspend fun toggle()


    @GET("plugin/ophom?action=checkplugstatus")
    suspend fun getState(): OphomState

}