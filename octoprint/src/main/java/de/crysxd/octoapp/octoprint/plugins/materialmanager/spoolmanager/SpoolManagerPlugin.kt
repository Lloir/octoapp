package de.crysxd.octoapp.octoprint.plugins.materialmanager.spoolmanager

import de.crysxd.octoapp.octoprint.plugins.materialmanager.Material
import de.crysxd.octoapp.octoprint.plugins.materialmanager.MaterialManagerPlugin

class SpoolManagerPlugin(private val spoolManagerApi: SpoolManagerApi) : MaterialManagerPlugin {
    override val pluginId = "SpoolManager"

    override suspend fun getMaterials(): List<Material> {
        val response = spoolManagerApi.listSpools()
        return response.allSpools.filter {
            // Hide spools that
            // - Are templates
            // - Are not active
            // - Are empty
            it.databaseId != null && it.isActive != false && (it.remainingWeight ?: 1f) > 0 && it.isTemplate != true
        }.map {
            Material(
                id = requireNotNull(it.databaseId) { "Database ID was validated to be not null" },
                displayName = it.displayName,
                color = it.color?.normalizeColor(),
                colorName = it.colorName,
                vendor = it.vendor ?: "Unknown",
                material = it.material ?: "Unknown",
                pluginDisplayName = "SpoolManager",
                pluginId = pluginId,
                pluginType = this::class,
                activeToolIndex = response.selectedSpools?.indexOfFirst { s -> s?.databaseId == it.databaseId },
                weightGrams = it.remainingWeight,
            )
        }
    }

    private fun String.normalizeColor() = "#${this.removePrefix("#").trim()}"

    override suspend fun activateSpool(id: String) = spoolManagerApi.selectSpool(SelectSpoolBody(id.toInt()))
}
