package de.crysxd.octoapp.octoprint.plugins.power.octolight

import de.crysxd.octoapp.octoprint.models.settings.Settings
import de.crysxd.octoapp.octoprint.plugins.power.PowerPlugin

class OctoLightPowerPlugin(
    private val octoLightApi: OctoLightApi
) : PowerPlugin<OctoLightPowerDevice> {

    internal suspend fun toggle() = octoLightApi.toggleLight()
    internal suspend fun turnOn() = octoLightApi.turnOn()
    internal suspend fun turnOff() = octoLightApi.turnOff()
    internal suspend fun getState() = octoLightApi.getState().state

    override fun getDevices(settings: Settings) = settings.plugins.filterValues {
        it is Settings.OctoLightSettings
    }.map {
        OctoLightPowerDevice(this)
    }
}