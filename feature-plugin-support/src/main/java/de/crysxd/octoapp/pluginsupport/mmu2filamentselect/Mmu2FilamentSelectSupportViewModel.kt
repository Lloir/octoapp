package de.crysxd.octoapp.pluginsupport.mmu2filamentselect

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.ext.shouldRetry
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.octoprint.models.settings.Settings.Mmu2FilamentSelect
import de.crysxd.octoapp.octoprint.models.settings.Settings.Mmu2FilamentSelect.LabelSource
import de.crysxd.octoapp.octoprint.models.settings.Settings.Mmu2FilamentSelect.LabelSource.Manual
import de.crysxd.octoapp.octoprint.models.socket.Message
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.flow.shareIn
import timber.log.Timber

class Mmu2FilamentSelectSupportViewModel(
    octoPrintProvider: OctoPrintProvider,
    private val octoPrintRepository: OctoPrintRepository,
) : ViewModel() {

    val events = octoPrintProvider.passiveCachedMessageFlow("mmu2-filament-select-support", Message.CompanionPluginMessage::class)
        .map { it?.mmuSelectionActive == true }
        .distinctUntilChanged()
        .map {
            Timber.i("MMU change: $it")
            val source = octoPrintRepository.getActiveInstanceSnapshot()?.settings?.plugins?.get(Mmu2FilamentSelect::class)?.labelSource ?: Manual
            when (it) {
                true -> Event.ShowMenu(source)
                false -> Event.CloseMenu
            }
        }.retry {
            Timber.e(it)
            delay(1000)
            it.shouldRetry()
        }.shareIn(viewModelScope, started = SharingStarted.Lazily, replay = 1)

    sealed class Event {
        data class ShowMenu(val labelSource: LabelSource) : Event()
        object CloseMenu : Event()
    }
}