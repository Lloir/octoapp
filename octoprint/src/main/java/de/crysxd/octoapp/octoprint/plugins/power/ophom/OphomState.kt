package de.crysxd.octoapp.octoprint.plugins.power.ophom

data class OphomState(
    val response: Int?,
    val reponse: Int?,
) {
    // Ophom has a typo in their JSON...I anticipate a fix and check for both
    val isOn get() = response == 1 || reponse == 1
}