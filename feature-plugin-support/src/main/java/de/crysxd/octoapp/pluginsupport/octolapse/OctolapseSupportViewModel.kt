package de.crysxd.octoapp.pluginsupport.octolapse

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.crysxd.baseui.OctoActivity
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.ext.shouldRetry
import de.crysxd.octoapp.base.ext.toHtml
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.octoprint.models.socket.Message
import de.crysxd.octoapp.octoprint.plugins.octolapse.OctolapseSnapshotPlanPreview
import de.crysxd.octoapp.octoprint.plugins.pluginmanager.PLUGIN_OCTOLAPSE
import de.crysxd.octoapp.pluginsupport.R
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.launch
import timber.log.Timber

class OctolapseSupportViewModel(
    private val octoPrintProvider: OctoPrintProvider,
    private val octoPrintRepository: OctoPrintRepository,
) : ViewModel() {

    private val mutableEvents = MutableSharedFlow<Event>(1)
    val events = mutableEvents.map { it }

    init {
        observeConnection()
        observeEvents()
    }

    private fun observeEvents() = viewModelScope.launch {
        octoPrintProvider.passiveCachedMessageFlow("octolapse-messages", Message.OctolapsePluginMessage::class)
            .filterNotNull()
            .onEach {
                it.errors?.firstOrNull()?.let { error ->
                    mutableEvents.tryEmit(
                        Event.ShowMessage(
                            OctoActivity.Message.DialogMessage(
                                title = { getString(R.string.octolapse___error___title) },
                                text = { listOf(error.name?.let { "<b>$it</b>" }, error.description).joinToString("<br><br>").toHtml() }
                            )
                        )
                    )
                }

                if (it.type == Message.OctolapsePluginMessage.Type.GcodePreProcessingStart) {
                    mutableEvents.tryEmit(Event.OpenBottomSheet(null))
                }
            }.saveCollect()
    }

    private fun observeConnection() = viewModelScope.launch {
        octoPrintProvider.passiveConnectionEventFlow("octolapse-conneciton").onEach {
            try {
                if (octoPrintRepository.getActiveInstanceSnapshot()?.hasPlugin(PLUGIN_OCTOLAPSE) == true) {
                    Timber.i("Connected, checking current Octolapse state")
                    octoPrintProvider.octoPrint().createOctolapseApi().getActiveSnapshotPlanPreview()?.let {
                        mutableEvents.tryEmit(Event.OpenBottomSheet(it))
                    }
                } else {
                    Timber.i("Octolapse not installed")
                }
            } catch (e: Exception) {
                // Nice try, we don't care. This is considered optional
                Timber.e(e, "Octolapse  state check failed")
            }
        }.saveCollect()
    }

    private suspend fun Flow<*>.saveCollect() = retry {
        delay(1000)
        Timber.e(it)
        it.shouldRetry()
    }.collect()

    sealed class Event {
        data class ShowMessage(val message: OctoActivity.Message.DialogMessage) : Event()
        data class OpenBottomSheet(val plan: OctolapseSnapshotPlanPreview?) : Event()
    }
}