package de.crysxd.octoapp.octoprint.plugins.octolapse

import com.google.gson.annotations.SerializedName

data class OctolapseStateAndSettings(
    @SerializedName("snapshot_plan_preview") val snapshotPlanPreview: OctolapseSnapshotPlanPreview?
)