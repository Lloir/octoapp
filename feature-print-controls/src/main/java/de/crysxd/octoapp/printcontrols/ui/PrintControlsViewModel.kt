package de.crysxd.octoapp.printcontrols.ui

import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import de.crysxd.baseui.BaseViewModel
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.usecase.CancelPrintJobUseCase
import de.crysxd.octoapp.base.usecase.TogglePausePrintJobUseCase
import de.crysxd.octoapp.base.usecase.execute
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class PrintControlsViewModel(
    octoPrintRepository: OctoPrintRepository,
    octoPrintProvider: OctoPrintProvider,
    private val togglePausePrintJobUseCase: TogglePausePrintJobUseCase,
    private val cancelPrintJobUseCase: CancelPrintJobUseCase,
) : BaseViewModel() {

    val printState = octoPrintProvider.passiveCurrentMessageFlow("printcontrols")
        .filter { it.progress != null }
        .asLiveData()

    val webCamSupported = octoPrintRepository.instanceInformationFlow()
        .map { it?.isWebcamSupported != false }
        .distinctUntilChanged()
        .asLiveData()

    fun togglePausePrint() = viewModelScope.launch(coroutineExceptionHandler) {
        togglePausePrintJobUseCase.execute()
    }

    fun cancelPrint() = viewModelScope.launch(coroutineExceptionHandler) {
        cancelPrintJobUseCase.execute(CancelPrintJobUseCase.Params(restoreTemperatures = false))
    }
}