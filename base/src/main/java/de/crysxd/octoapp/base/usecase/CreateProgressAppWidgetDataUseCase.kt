package de.crysxd.octoapp.base.usecase

import android.os.Parcelable
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.octoprint.exceptions.PrinterNotOperationalException
import de.crysxd.octoapp.octoprint.models.socket.Message
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext
import kotlinx.parcelize.Parcelize
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class CreateProgressAppWidgetDataUseCase @Inject constructor(
    private val octoPrintRepository: OctoPrintRepository,
    private val octoPrintProvider: OctoPrintProvider
) : UseCase<CreateProgressAppWidgetDataUseCase.Params, CreateProgressAppWidgetDataUseCase.Result>() {

    init {
        suppressLogging = true
    }

    override suspend fun doExecute(param: Params, timber: Timber.Tree) =
        param.currentMessage?.let { fromCurrentMessage(it, param.instanceId) }
            ?: fromNetworkRequest(param.instanceId)

    private suspend fun fromNetworkRequest(instanceId: String): Result = withContext(Dispatchers.IO) {
        val instance = octoPrintRepository.get(instanceId) ?: throw IllegalStateException("Unable to locate instance for $instanceId")
        val octoPrint = octoPrintProvider.octoPrint(instance.id)
        val asyncJob = async { octoPrint.createJobApi().getJob() }
        val asyncState = async {
            try {
                octoPrint.createPrinterApi().getPrinterState()
            } catch (e: PrinterNotOperationalException) {
                // Printer not connected, null state will indicate this
                null
            }
        }

        val state = asyncState.await()
        val job = asyncJob.await()

        return@withContext Result(
            isPrinting = state?.state?.flags?.printing == true,
            isPausing = state?.state?.flags?.pausing == true,
            isCancelling = state?.state?.flags?.cancelling == true,
            isPaused = state?.state?.flags?.paused == true,
            isPrinterConnected = state != null,
            isLive = false,
            printProgress = job.progress?.completion?.let { it / 100f },
            printTimeLeft = job.progress?.printTimeLeft,
            printTimeLeftOrigin = job.progress?.printTimeLeftOrigin,
            instanceId = instance.id,
            label = instance.label,
        )
    }

    private fun fromCurrentMessage(currentMessage: Message.CurrentMessage, instanceId: String) = Result(
        isPrinting = currentMessage.state?.flags?.printing == true,
        isPausing = currentMessage.state?.flags?.pausing == true,
        isCancelling = currentMessage.state?.flags?.cancelling == true,
        isPaused = currentMessage.state?.flags?.paused == true,
        isLive = true,
        isPrinterConnected = true,
        printProgress = currentMessage.progress?.completion?.let { it / 100f },
        printTimeLeft = currentMessage.progress?.printTimeLeft,
        printTimeLeftOrigin = currentMessage.progress?.printTimeLeftOrigin,
        instanceId = instanceId,
        label = octoPrintRepository.get(instanceId)?.label ?: instanceId
    )

    data class Params(
        val currentMessage: Message.CurrentMessage?,
        val instanceId: String
    )

    @Parcelize
    data class Result(
        val isPrinting: Boolean,
        val isPausing: Boolean,
        val isPaused: Boolean,
        val isCancelling: Boolean,
        val isPrinterConnected: Boolean,
        val isLive: Boolean,
        val printProgress: Float?,
        val printTimeLeft: Int?,
        val printTimeLeftOrigin: String?,
        val instanceId: String,
        val label: String,
        val createdAt: Date = Date(),
    ) : Parcelable
}