package de.crysxd.baseui.common

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.annotation.DrawableRes
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.appcompat.widget.AppCompatImageButton
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.GravityCompat
import androidx.core.view.ViewCompat
import androidx.core.view.children
import androidx.core.view.isVisible
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.fragment.app.findFragment
import androidx.lifecycle.lifecycleScope
import androidx.transition.TransitionManager
import de.crysxd.baseui.R
import de.crysxd.baseui.databinding.BottomToolbarViewBinding
import de.crysxd.baseui.utils.InstantAutoTransition
import de.crysxd.octoapp.base.di.BaseInjector
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import timber.log.Timber
import kotlin.math.absoluteValue

class BottomToolbarView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) : ConstraintLayout(context, attrs) {

    private var rightHanded = !BaseInjector.get().octoPreferences().leftHandMode
    private val binding = BottomToolbarViewBinding.inflate(LayoutInflater.from(context), this)
    private val initialFlowViewCount = binding.flow.referencedIds.size
    val menuButton get() = binding.menu
    private var collectJob: Job? = null

    init {
        elevation = resources.getDimension(R.dimen.margin_1)
        updatePadding(
            top = resources.getDimensionPixelSize(R.dimen.margin_0_1),
            bottom = resources.getDimensionPixelSize(R.dimen.margin_0_1)
        )
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        // Sort flow in case lefty mode was changed
        try {
            collectJob?.cancel()
            collectJob = findFragment<Fragment>().viewLifecycleOwner.lifecycleScope.launchWhenCreated {
                BaseInjector.get().octoPreferences().updatedFlow.collect {
                    val r = !BaseInjector.get().octoPreferences().leftHandMode
                    if (rightHanded != r) {
                        rightHanded = r
                        sortFlow()
                    }
                }
            }
        } catch (e: Exception) {
            // This will fail from time to time....but it's not super important
            Timber.w("Unable to collect settings (${e::class.simpleName}: ${e.message})")
            collectJob?.cancel()
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        collectJob?.cancel()
    }

    @SuppressLint("ClickableViewAccessibility")
    fun addAction(@DrawableRes icon: Int, @StringRes title: Int, @IdRes id: Int, needsSwipe: Boolean, action: () -> Unit): List<View> {
        val group = mutableListOf<View>()

        if (binding.flow.referencedIds.size > initialFlowViewCount) {
            val separator = View(context)
            separator.id = ViewCompat.generateViewId()
            separator.setBackgroundResource(R.color.input_background)
            addView(
                separator,
                ViewGroup.LayoutParams(
                    resources.getDimension(R.dimen.bottom_toolbar_separator_width).toInt(),
                    resources.getDimension(R.dimen.bottom_toolbar_separator_height).toInt(),
                )
            )
            binding.flow.addView(separator)
            group.add(separator)
        }

        val button = AppCompatImageButton(ContextThemeWrapper(context, R.style.OctoTheme_Widget_Button_Image))
        button.setImageResource(icon)
        button.id = id
        button.contentDescription = context.getString(title)
        addView(button)
        binding.flow.addView(button)
        group.add(button)

        if (needsSwipe) {
            button.setOnTouchListener { _, event ->
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        requireSwipeButtons().startSwipeButton(
                            icon = icon,
                            label = context.getString(title),
                            button = button,
                            action = action,
                            rightHanded = rightHanded
                        )
                    }

                    else -> {
                        event.setLocation(event.x + button.x, event.y)
                        requireSwipeButtons().onTouchEvent(event)
                    }
                }

                true
            }
        } else {
            button.setOnClickListener {
                action()
            }
        }

        sortFlow()
        return group
    }

    private fun sortFlow() {
        val copy = binding.flow.referencedIds.toMutableList()
        val idIn = binding.flow.referencedIds.toMutableList()
        idIn.remove(R.id.main)
        idIn.remove(R.id.menu)
        idIn.remove(R.id.bufferStart)
        idIn.remove(R.id.status)
        val idOut = mutableListOf<Int>()

        if (rightHanded) {
            idOut.add(R.id.bufferStart)
            idOut.add(R.id.main)
            idOut.addAll(idIn)
            idOut.add(R.id.status)
            idOut.add(R.id.menu)

            binding.status.gravity = GravityCompat.END
            binding.status.translationX = binding.status.translationX.absoluteValue
            binding.menu.translationX = binding.menu.translationX.absoluteValue
            binding.menu.scaleX = 1f
        } else {
            idOut.add(R.id.menu)
            idOut.add(R.id.status)
            idOut.addAll(idIn)
            idOut.add(R.id.main)
            idOut.add(R.id.bufferStart)

            binding.status.gravity = GravityCompat.START
            binding.status.translationX = binding.status.translationX.absoluteValue * -1
            binding.menu.translationX = binding.menu.translationX.absoluteValue * -1
            binding.menu.scaleX = -1f
        }

        // Update reference ids...for some reason we need to remove and add the view to apply it
        binding.flow.referencedIds = idOut.toIntArray()
        removeView(binding.flow)
        addView(binding.flow)
    }

    fun setStatus(status: CharSequence?) {
        binding.status.text = status
    }

    fun startDelayedTransition() = TransitionManager.beginDelayedTransition(this, InstantAutoTransition(changeBounds = false, fadeText = false))

    fun setMainAction(@StringRes text: Int): Button {
        binding.main.setText(text)
        binding.main.isVisible = true
        return binding.main
    }

    private fun requireSwipeButtons() = (parent as ViewGroup).children.firstOrNull { it.id == R.id.swipeButtons } as? BottomToolbarSwipeButtonView
        ?: throw IllegalStateException("No swipe buttons")
}
