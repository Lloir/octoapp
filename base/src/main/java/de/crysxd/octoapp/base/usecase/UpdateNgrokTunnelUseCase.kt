package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.octoprint.isNgrokUrl
import de.crysxd.octoapp.octoprint.models.settings.Settings
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import timber.log.Timber
import javax.inject.Inject

class UpdateNgrokTunnelUseCase @Inject constructor(
    private val octoPrintProvider: OctoPrintProvider,
    private val octoPrintRepository: OctoPrintRepository,
) : UseCase<UpdateNgrokTunnelUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, timber: Timber.Tree) {
        val tunnel = (param as? Params.Tunnel)?.tunnel
            ?: octoPrintProvider.octoPrint().createNgrokApi().getActiveTunnel().tunnel
        val setting = octoPrintRepository.getActiveInstanceSnapshot()?.settings?.plugins?.get(Settings.Ngrok::class)
            ?: octoPrintProvider.octoPrint().createSettingsApi().getSettings().plugins[Settings.Ngrok::class]
        val tunnelUrl = tunnel?.let {
            (it.toHttpUrlOrNull() ?: "https://$it".toHttpUrlOrNull())
                ?.newBuilder()
                ?.username(setting?.authName ?: "")
                ?.password(setting?.authPassword ?: "")
                ?.build()
        }

        octoPrintRepository.updateActive {
            // We only update if we have no remote connection or it's already a ngrok URL
            if (it.alternativeWebUrl == null || it.alternativeWebUrl.isNgrokUrl()) {
                Timber.i("Update ngrok tunnel to $tunnelUrl")
                it.copy(alternativeWebUrl = tunnelUrl, octoEverywhereConnection = null)
            } else {
                Timber.i("Not configured for ngrok, will not touch")
                it
            }
        }
    }

    sealed class Params {
        object FetchConfig : Params()
        data class Tunnel(val tunnel: String?) : Params()
    }
}