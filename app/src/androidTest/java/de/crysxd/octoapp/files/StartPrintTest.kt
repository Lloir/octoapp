package de.crysxd.octoapp.files

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.swipeUp
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.platform.app.InstrumentationRegistry
import com.adevinta.android.barista.rule.BaristaRule
import com.adevinta.android.barista.rule.flaky.AllowFlaky
import de.crysxd.octoapp.MainActivity
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.framework.BottomToolbarRobot
import de.crysxd.octoapp.framework.MenuRobot
import de.crysxd.octoapp.framework.MenuRobot.checkHasMenuButton
import de.crysxd.octoapp.framework.TestEnvironmentLibrary
import de.crysxd.octoapp.framework.WorkspaceRobot
import de.crysxd.octoapp.framework.ext.setActive
import de.crysxd.octoapp.framework.rules.AutoConnectPrinterRule
import de.crysxd.octoapp.framework.rules.IdleTestEnvironmentRule
import de.crysxd.octoapp.framework.rules.ResetDaggerRule
import de.crysxd.octoapp.framework.rules.TestDocumentationRule
import de.crysxd.octoapp.framework.waitFor
import de.crysxd.octoapp.framework.waitForDialog
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain

class StartPrintTest {

    private val testEnvVanilla = TestEnvironmentLibrary.Terrier
    private val testEnvSpoolManager = TestEnvironmentLibrary.Frenchie
    private val baristaRule = BaristaRule.create(MainActivity::class.java)

    @get:Rule
    val chain = RuleChain.outerRule(baristaRule)
        .around(IdleTestEnvironmentRule(testEnvSpoolManager, testEnvVanilla))
        .around(TestDocumentationRule())
        .around(ResetDaggerRule())
        .around(AutoConnectPrinterRule())

    @Test(timeout = 120_000)
    @AllowFlaky(attempts = 3)
    fun WHEN_a_print_is_started_THEN_the_app_shows_printing() {
        // GIVEN
        BaseInjector.get().octorPrintRepository().setActive(testEnvVanilla)
        baristaRule.launchActivity()

        // Open file and start print
        triggerPrint("layers.gcode")

        // Wait for print workspace to be shown
        verifyPrinting("layers.gcode")

        // Pause
        waitFor(allOf(withId(R.id.status), withText("1 %"), isDisplayed()), timeout = 60_000)
        onView(withId(R.id.pause_print)).check(matches(isDisplayed()))
        onView(withId(R.id.cancel_print)).check(matches(isDisplayed()))
        onView(withId(R.id.resume_print)).check(matches(not(isDisplayed())))
        BottomToolbarRobot.confirmButtonWithSwipe(R.id.pause_print, baristaRule)

        // Wait for paused and resume
        waitFor(allOf(withText(R.string.pausing), isDisplayed()))
        waitFor(allOf(withText(R.string.paused), isDisplayed()), timeout = 45_000)
        onView(withId(R.id.pause_print)).check(matches(not(isDisplayed())))
        onView(withId(R.id.cancel_print)).check(matches(isDisplayed()))
        onView(withId(R.id.resume_print)).check(matches(isDisplayed()))
        BottomToolbarRobot.confirmButtonWithSwipe(R.id.resume_print, baristaRule)

        // Wait for resume and cancel
        waitFor(allOf(withId(R.id.status), withText("2 %"), isDisplayed()), timeout = 45_000)
        onView(withId(R.id.pause_print)).check(matches(isDisplayed()))
        onView(withId(R.id.cancel_print)).check(matches(isDisplayed()))
        onView(withId(R.id.resume_print)).check(matches(not(isDisplayed())))
        BottomToolbarRobot.confirmButtonWithSwipe(R.id.cancel_print, baristaRule)
        WorkspaceRobot.waitForPrepareWorkspace()
    }

    @Test(timeout = 60_000)
    @AllowFlaky(attempts = 3)
    fun WHEN_a_print_is_started_and_a_spool_is_selected_with_SpoolManager_THEN_the_app_shows_printing() =
        runMaterialTest("SM Spätzle")

    @Test(timeout = 60_000)
    @AllowFlaky(attempts = 3)
    fun WHEN_a_print_is_started_and_a_spool_is_selected_with_Filament_Manager_THEN_the_app_shows_printing() =
        runMaterialTest("FM Fusili (ABS)")

    @Test(timeout = 60_000)
    @AllowFlaky(attempts = 3)
    fun WHEN_a_print_is_started_and_no_spool_is_selected_THEN_the_app_shows_printing() =
        runMaterialTest(InstrumentationRegistry.getInstrumentation().targetContext.getString(R.string.material_menu___print_without_selection))

    private fun runMaterialTest(selection: String) {
        // GIVEN
        BaseInjector.get().octorPrintRepository().setActive(testEnvSpoolManager)
        baristaRule.launchActivity()

        // Open file and start print
        // Use waits.gcode to NOT use material, changing the spool weights
        triggerPrint("waits.gcode")

        // Check dialog
        verifyMaterialSelection()
        onView(withText(selection)).inRoot(isDialog()).perform(click())

        // Wait for print workspace to be shown
        MenuRobot.waitForMenuToBeClosed()
        verifyPrinting("waits.gcode")
    }

    private fun triggerPrint(file: String) {
        WorkspaceRobot.waitForPrepareWorkspace()
        onView(withText(R.string.start_printing)).perform(click())
        onView(withId(R.id.recyclerViewFileList)).perform(swipeUp())
        waitFor(allOf(withText(file), isDisplayed()))
        onView(withText(file)).perform(click())
        waitFor(allOf(withText(R.string.start_printing), isDisplayed()))
        onView(withText(R.string.start_printing)).perform(click())
    }

    private fun verifyPrinting(file: String) {
        // Wait for print workspace
        WorkspaceRobot.waitForPrintWorkspace()

        // Wait for print data to show up
        waitFor(allOf(withText(R.string.less_than_a_minute), isDisplayed()), timeout = 10_000)
        waitFor(allOf(withText(file), isDisplayed()), timeout = 10_000)
    }

    private fun verifyMaterialSelection() {
        waitForDialog(withText(R.string.material_menu___title_select_material))
        checkHasMenuButton(text = "FM Fusili (ABS)", index = 0, rightDetail = "820g")
        checkHasMenuButton(text = "SM Spätzle", index = 1, rightDetail = "99g")
        checkHasMenuButton(text = "FM Fusili (PLA)", index = 2, rightDetail = "1000g")
        checkHasMenuButton(text = "SM Ramen (PLA, Japan)", index = 3, rightDetail = "170g")
        checkHasMenuButton(text = "SM Ramen (PLA, Japan)", index = 4, rightDetail = "1000g")
        checkHasMenuButton(text = "SM Spaghetti", index = 5, rightDetail = "90g")
        checkHasMenuButton(text = "Template", index = 0, false)
        checkHasMenuButton(text = "Empty", index = 0, false)
        checkHasMenuButton(text = "Not Active", index = 0, false)
        checkHasMenuButton(InstrumentationRegistry.getInstrumentation().targetContext.getString(R.string.material_menu___print_without_selection), 6)
    }
}