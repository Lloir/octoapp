package de.crysxd.octoapp.octoprint.plugins.power.octolight

data class OctoLightState(
    val state: Boolean?
)
