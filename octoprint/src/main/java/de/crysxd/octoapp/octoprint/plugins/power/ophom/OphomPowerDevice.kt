package de.crysxd.octoapp.octoprint.plugins.power.ophom

import de.crysxd.octoapp.octoprint.plugins.power.PowerDevice

data class OphomPowerDevice(private val plugin: OphomPowerPlugin) : PowerDevice() {
    override val id = "ophom"
    override val pluginId = "ophom"
    override val displayName = "Ophom Light"
    override val pluginDisplayName = "Ophom"
    override val capabilities
        get() = listOf(Capability.Illuminate, Capability.ControlPrinterPower)
    override val controlMethods: List<ControlMethod>
        get() = listOf(ControlMethod.Toggle, ControlMethod.TurnOnOff)

    override suspend fun turnOn() = if (isOn()) Unit else toggle()
    override suspend fun turnOff() = if (isOn()) toggle() else Unit
    override suspend fun toggle() = plugin.toggle()
    override suspend fun isOn() = plugin.getState()
}