package de.crysxd.octoapp.octoprint.json

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter

class SafeFloatAdapter : TypeAdapter<Float>() {
    override fun write(out: JsonWriter, value: Float?) {
        out.value(value)
    }

    override fun read(reader: JsonReader): Float? = when (reader.peek()) {
        JsonToken.NUMBER -> reader.nextDouble().toFloat()
        JsonToken.STRING -> reader.nextString().toFloatOrNull()
        else -> {
            reader.skipValue()
            null
        }
    }
}