package de.crysxd.octoapp.octoprint.exceptions

import okhttp3.HttpUrl

class OctoPrintNotFoundException(httpUrl: HttpUrl, body: String) : OctoPrintApiException(httpUrl = httpUrl, responseCode = 404, body = body)