package de.crysxd.octoapp.octoprint.exceptions

import okhttp3.HttpUrl

class InvalidTimelapseConfigException(httpUrl: HttpUrl, responseCode: Int, body: String) : OctoPrintException(
    webUrl = httpUrl,
    technicalMessage = "Received $responseCode when updating timelapse: $body",
    userFacingMessage = "The timelapse configuration you tried to save was not accepted by OctoPrint."
)
