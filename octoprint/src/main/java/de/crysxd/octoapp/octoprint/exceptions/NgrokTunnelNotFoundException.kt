package de.crysxd.octoapp.octoprint.exceptions

import okhttp3.HttpUrl

class NgrokTunnelNotFoundException(webUrl: HttpUrl) : OctoPrintException(
    userFacingMessage = "The ngrok tunnel is gone and was removed. This can happen when you are using the free ngrok tier and restart your OctoPrint.\n\nWhen OctoApp can reach your OctoPrint again, the new tunnel URL will be fetched automatically.",
    webUrl = webUrl,
), RemoteServiceConnectionBrokenException {
    override val remoteServiceName = "ngrok"
}