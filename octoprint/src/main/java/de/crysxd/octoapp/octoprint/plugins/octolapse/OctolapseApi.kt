package de.crysxd.octoapp.octoprint.plugins.octolapse

import retrofit2.http.Body
import retrofit2.http.POST

interface OctolapseApi {

    @POST("plugin/octolapse/loadSettingsAndState")
    suspend fun getSettingsAndState(): OctolapseStateAndSettings

    @POST("plugin/octolapse/cancelPreprocessing")
    suspend fun cancelPreprocessing(@Body body: OctolapseCommandBody): OctolapseStateAndSettings

    @POST("plugin/octolapse/acceptSnapshotPlanPreview")
    suspend fun acceptSnapshotPlan(@Body body: OctolapseCommandBody): OctolapseStateAndSettings

    class Wrapper(private val api: OctolapseApi) {

        suspend fun getActiveSnapshotPlanPreview() = api.getSettingsAndState().snapshotPlanPreview

        suspend fun cancelPreprocessing(jobId: String?) = api.cancelPreprocessing(OctolapseCommandBody(cancel = true, jobId = jobId))

        suspend fun acceptSnapshotPlan(jobId: String?) = api.acceptSnapshotPlan(OctolapseCommandBody(jobId = jobId))
    }
}