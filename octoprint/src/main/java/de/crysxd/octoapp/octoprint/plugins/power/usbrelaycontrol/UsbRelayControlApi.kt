package de.crysxd.octoapp.octoprint.plugins.power.usbrelaycontrol

import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface UsbRelayControlApi {

    @POST("plugin/usbrelaycontrol")
    suspend fun sendCommand(@Body command: UsbRelayControlCommand): Response<Unit>

    @GET("plugin/usbrelaycontrol")
    suspend fun getGpioState(): Array<RelayState>

}