package de.crysxd.octoapp.octoprint.plugins.thespaghettidetective

data class SpaghettiCamFrame(
    val snapshot: String?
)
