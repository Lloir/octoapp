package de.crysxd.octoapp.octoprint.exceptions

class InvalidPathException : IllegalArgumentException("Unable to resolve path"), SuppressedException