package de.crysxd.octoapp.octoprint.plugins.power.octohue

import de.crysxd.octoapp.octoprint.plugins.power.PowerDevice

data class OctoHuePowerDevice(private val plugin: OctoHuePowerPlugin) : PowerDevice() {
    override val id = "octohue"
    override val pluginId = "octohue"
    override val displayName = "OctoHue Light"
    override val pluginDisplayName = "OctoHue"
    override val capabilities
        get() = listOf(Capability.Illuminate)
    override val controlMethods: List<ControlMethod>
        get() = listOf(ControlMethod.Toggle)

    override suspend fun turnOn() = throw UnsupportedOperationException("Not supported")
    override suspend fun turnOff() = throw UnsupportedOperationException("Not supported")
    override suspend fun toggle() = plugin.toggle()
    override suspend fun isOn(): Boolean? = null
}