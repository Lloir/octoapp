package de.crysxd.octoapp.octoprint.plugins.power.enclosure

import com.google.gson.annotations.SerializedName
import de.crysxd.octoapp.octoprint.plugins.power.PowerDevice

data class EnclosureOutputPowerDevice(
    private val plugin: EnclosureOutputPowerPlugin? = null,
    @SerializedName("label") override val displayName: String,
    @SerializedName("output_type") val type: String,
    @SerializedName("index_id") val indexId: Int
) : PowerDevice() {
    override val id = "index-$indexId"
    override val pluginId = "enclosure"
    override val pluginDisplayName = "Enclosure Plugin"
    override val capabilities
        get() = listOf(Capability.ControlPrinterPower, Capability.Illuminate)

    override suspend fun turnOn() = plugin?.turnOn(this) ?: throw IllegalStateException("Acquire this class from EnclosureOutputPowerPlugin")
    override suspend fun turnOff() = plugin?.turnOff(this) ?: throw IllegalStateException("Acquire this class from EnclosureOutputPowerPlugin")
    override suspend fun isOn(): Boolean = plugin?.isOn(this) ?: throw IllegalStateException("Acquire this class from EnclosureOutputPowerPlugin")
}
