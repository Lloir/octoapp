package de.crysxd.octoapp.pluginsupport.octolapse

import android.graphics.Color
import android.graphics.Paint
import android.graphics.PointF
import androidx.lifecycle.viewModelScope
import de.crysxd.baseui.BaseViewModel
import de.crysxd.baseui.OctoActivity
import de.crysxd.octoapp.base.data.models.GcodePreviewSettings
import de.crysxd.octoapp.base.gcode.parse.models.Move
import de.crysxd.octoapp.base.gcode.render.GcodeRenderView
import de.crysxd.octoapp.base.gcode.render.models.GcodePath
import de.crysxd.octoapp.base.gcode.render.models.GcodeRenderContext
import de.crysxd.octoapp.base.gcode.render.models.RenderStyle
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.usecase.GenerateRenderStyleUseCase
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.octoprint.models.socket.Message
import de.crysxd.octoapp.octoprint.plugins.octolapse.OctolapseSnapshotPlanPreview
import de.crysxd.octoapp.pluginsupport.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import timber.log.Timber

class OctolapseBottomSheetViewModel(
    private val octoPrintProvider: OctoPrintProvider,
    private val generateRenderStyleUseCase: GenerateRenderStyleUseCase,
) : BaseViewModel() {

    private val initialMoveType = Move.Type.Extrude
    private val snapshotMoveType = Move.Type.Travel
    private val returnMoveType = Move.Type.Unsupported
    private var activeJobId: String? = null

    private val mutableEvents = MutableSharedFlow<OctolapseState>(1)
    val events = mutableEvents.onEach {
        activeJobId = (it as? OctolapseState.SnapshotPlan)?.jobId
    }

    private val flow = octoPrintProvider
        .passiveCachedMessageFlow("octolapse-bottomsheet", Message.OctolapsePluginMessage::class)
        .filterNotNull()
        .map {
            Timber.i("Octolapse message: ${it.type}")

            when {
                it.errors?.firstOrNull() != null -> OctolapseState.Close
                it.snapshotPlanPreview != null -> OctolapseState.SnapshotPlan(
                    layers = convertSnapshotPlanToGcode(it.snapshotPlanPreview?.snapshotPlans!!),
                    jobId = it.snapshotPlanPreview?.jobId
                )
                it.type == Message.OctolapsePluginMessage.Type.SnapshotPlanComplete -> OctolapseState.Close
                it.type == Message.OctolapsePluginMessage.Type.GcodePreProcessingUpdate -> OctolapseState.Loading(R.string.octolapse___snapshot_plan___title_analyzing)
                it.type == Message.OctolapsePluginMessage.Type.GcodePreProcessingStart -> OctolapseState.Loading(R.string.octolapse___snapshot_plan___title_analyzing)
                else -> null
            }
        }
        .filterNotNull()
        .flowOn(Dispatchers.Default)
        .distinctUntilChanged()


    init {
        viewModelScope.launch {
            flow.catch { Timber.e(it) }.collect { mutableEvents.tryEmit(it) }
        }
    }

    fun consumePlan(planPreview: OctolapseSnapshotPlanPreview) = viewModelScope.launch(Dispatchers.Default) {
        try {
            val layers = planPreview.snapshotPlans?.let { convertSnapshotPlanToGcode(it) } ?: emptyList()
            mutableEvents.tryEmit(OctolapseState.SnapshotPlan(layers, planPreview.jobId))
        } catch (e: Exception) {
            Timber.e(e)
            mutableEvents.tryEmit(OctolapseState.Close)
        }
    }

    private suspend fun convertSnapshotPlanToGcode(snapshotsPlan: OctolapseSnapshotPlanPreview.SnapshotPlanList): List<OctolapseState.Layer>? {
        val style = createRenderStyle()
        val width = snapshotsPlan.volume?.width ?: return null
        val height = snapshotsPlan.volume?.height ?: return null
        val origin = snapshotsPlan.volume?.origin ?: return null
        val snapshots = snapshotsPlan.snapshotPlans ?: return null
        val baseRadius = width * 0.01f

        return snapshots.mapIndexed { i, snap ->
            val initialPosition = snap.initialPosition.toPointF()
            val snapshotPosition = snap.steps?.takeWhile { it.action != "snapshot" }?.findLast { it.action == "travel" }.toPointF()
            val returnPosition = snap.initialPosition.toPointF()
            val renderContext = GcodeRenderContext(
                previousLayerPaths = null,
                completedLayerPaths = listOfNotNull(
                    initialPosition?.let { createArcMove(baseRadius * 6, it.x, it.y, initialMoveType) },
                    snapshotPosition?.let { createArcMove(baseRadius * 4, it.x, it.y, snapshotMoveType) },
                    returnPosition?.let { createArcMove(baseRadius * 2, it.x, it.y, returnMoveType) },
                ),
                remainingLayerPaths = null,
                printHeadPosition = null,
                layerCount = snapshots.size,
                layerNumber = i,
                layerProgress = 0f,
                layerZHeight = 0f,
            )
            val params = GcodeRenderView.RenderParams(
                renderContext = renderContext,
                renderStyle = style,
                printBedSizeMm = PointF(width, height),
                originInCenter = origin == OctolapseSnapshotPlanPreview.Origin.Center,
                extrusionWidthMm = width * 0.1f,
                quality = GcodePreviewSettings.Quality.Ultra
            )
            OctolapseState.Layer(
                renderParams = params,
                initialPosition = initialPosition,
                returnPosition = returnPosition,
                snapshotPosition = snapshotPosition,
                layerNumber = i,
                layerCount = snapshots.size
            )
        }.takeIf { it.isNotEmpty() }
    }

    private fun OctolapseSnapshotPlanPreview.Position?.toPointF(): PointF? {
        val x = this?.x ?: return null
        val y = this.y ?: return null
        return PointF(x, y)
    }

    private fun OctolapseSnapshotPlanPreview.Step?.toPointF(): PointF? {
        val x = this?.x ?: return null
        val y = this.y ?: return null
        return PointF(x, y)
    }

    private fun createArcMove(radius: Float, x: Float?, y: Float?, type: Move.Type): GcodePath? {
        x ?: return null
        y ?: return null

        val move = Move.ArcMove(
            positionInFile = 0,
            leftX = x - radius,
            topY = y - radius,
            startAngle = 0f,
            sweepAngle = 360f,
            r = radius
        )

        return GcodePath(
            arcs = listOf(move),
            lines = FloatArray(0),
            linesCount = 0,
            linesOffset = 0,
            moveCount = 1,
            type = type
        )
    }

    fun accept() = viewModelScope.launch(coroutineExceptionHandler) {
        // No job? Close right away
        val jobId = requireNotNull(activeJobId) { "No render job ID available" }
        val state = mutableEvents.replayCache.firstOrNull() ?: OctolapseState.Loading(0)
        try {
            mutableEvents.tryEmit(OctolapseState.Loading(0))
            octoPrintProvider.octoPrint().createOctolapseApi().acceptSnapshotPlan(jobId)
            activeJobId = null
            mutableEvents.tryEmit(OctolapseState.Close)
        } catch (e: Exception) {
            // Restore
            mutableEvents.tryEmit(state)
            throw e
        }
    }

    fun onDismiss() = AppScope.launch {
        try {
            // Use app scope, fragment is dismissed
            // No job? Either not done loading or already accepted
            activeJobId?.let {
                postMessage(
                    OctoActivity.Message.SnackbarMessage(
                        text = { it.getString(R.string.cancelling) },
                        type = OctoActivity.Message.SnackbarMessage.Type.Positive
                    )
                )
                octoPrintProvider.octoPrint().createOctolapseApi().cancelPreprocessing(activeJobId)
            }
        } catch (e: Exception) {
            Timber.e(e)
        }
    }

    private suspend fun createRenderStyle() = RenderStyle(
        previousLayerPaint = Paint(),
        printHeadPaint = Paint(),
        remainingLayerPaint = Paint(),
        background = generateRenderStyleUseCase.execute(null).background,
        paintPalette = {
            Paint().apply {
                style = Paint.Style.FILL
                color = when (it) {
                    initialMoveType -> Color.parseColor("#EE3F3F").withAlpha(0.6f)
                    snapshotMoveType -> Color.parseColor("#389AE5")
                    returnMoveType -> Color.parseColor("#27AE60")
                    else -> Color.TRANSPARENT
                }
            }
        }
    )

    private fun Int.withAlpha(alpha: Float) = Color.argb(
        (alpha * 255).toInt(),
        Color.red(this),
        Color.green(this),
        Color.blue(this)
    )
}