package de.crysxd.octoapp.octoprint.plugins.ngrok

import retrofit2.http.GET

interface NgrokApi {

    @GET("plugin/ngrok")
    suspend fun getActiveTunnel(): NgrokConfig
}