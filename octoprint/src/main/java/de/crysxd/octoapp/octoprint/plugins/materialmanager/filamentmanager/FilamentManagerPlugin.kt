package de.crysxd.octoapp.octoprint.plugins.materialmanager.filamentmanager

import de.crysxd.octoapp.octoprint.plugins.materialmanager.Material
import de.crysxd.octoapp.octoprint.plugins.materialmanager.MaterialManagerPlugin
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext

class FilamentManagerPlugin(private val filamentManagerApi: FilamentManagerApi) : MaterialManagerPlugin {
    override val pluginId = "filamentmanager"

    private val colorLut = mapOf(
        "White" to "#ffffff",
        "Yellow" to "#ffd500",
        "Green" to "#07e328",
        "Blue" to "#07e328",
        "Gray" to "#8c8c8c",
        "Grey" to "#8c8c8c",
        "Magenta" to "#fc19d7",
        "Purple" to "#ab008f",
        "Teal" to "#04c9c6",
        "Pink" to "#ff00c3",
        "Black" to "#000000",
        "Natural" to "#e5e6d5",
        "Transparent" to "#bfbfbf",
        "Fusili" to "#00ff00",
    )

    private fun String.findColor() =
        colorLut.entries.firstOrNull { contains(it.key, ignoreCase = true) }

    override suspend fun getMaterials() = withContext(Dispatchers.IO) {
        val spools = async { filamentManagerApi.listSpools().spools }
        val selection = filamentManagerApi.getSelections()

        spools.await().filter { (it.weight ?: 0f) > 0 }.map { spool ->
            val color = spool.name.findColor()
            Material(
                id = spool.id,
                displayName = spool.name,
                color = color?.value,
                colorName = color?.key,
                vendor = spool.profile.vendor ?: "Unknown",
                material = spool.profile.material ?: "Unknown",
                pluginDisplayName = "FilamentManager",
                pluginId = pluginId,
                pluginType = this@FilamentManagerPlugin::class,
                activeToolIndex = selection.selections.firstOrNull { it?.spool?.id == spool.id }?.tool,
                weightGrams = spool.weight?.let { w -> spool.used?.let { u -> w - u } },
            )
        }
    }


    override suspend fun activateSpool(id: String) = filamentManagerApi.selectSpool(
        SelectSpoolBody(
            SelectSpoolBody.Selection(
                tool = 0,
                spool = SelectSpoolBody.Spool(id)
            )
        )
    )
}