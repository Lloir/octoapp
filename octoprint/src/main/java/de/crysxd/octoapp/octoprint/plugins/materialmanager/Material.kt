package de.crysxd.octoapp.octoprint.plugins.materialmanager

import kotlin.reflect.KClass

data class Material(
    val id: String,
    val pluginId: String,
    val pluginType: KClass<out MaterialManagerPlugin>,
    val displayName: String,
    val vendor: String,
    val material: String,
    val color: String?,
    val colorName: String?,
    val pluginDisplayName: String,
    val activeToolIndex: Int?,
    val weightGrams: Float?,
) {
    val uniqueId
        get() = "$pluginId:$id"

    val isActivated
        get() = activeToolIndex?.takeIf { it >= 0 } != null
}