package de.crysxd.octoapp.octoprint.plugins.ngrok

data class NgrokConfig(
    val tunnel: String?,
)