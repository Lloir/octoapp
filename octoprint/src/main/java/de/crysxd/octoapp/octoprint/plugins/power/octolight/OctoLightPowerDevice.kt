package de.crysxd.octoapp.octoprint.plugins.power.octolight

import de.crysxd.octoapp.octoprint.plugins.power.PowerDevice

data class OctoLightPowerDevice(private val plugin: OctoLightPowerPlugin) : PowerDevice() {
    override val id = "octolight"
    override val pluginId = "octolight"
    override val displayName = "OctoLight"
    override val pluginDisplayName = "OctoLight 0.1.3"
    override val capabilities
        get() = listOf(Capability.Illuminate)
    override val controlMethods: List<ControlMethod>
        get() = listOf(ControlMethod.Toggle, ControlMethod.TurnOnOff)

    override suspend fun turnOn() = plugin.turnOn()
    override suspend fun turnOff() = plugin.turnOff()
    override suspend fun toggle() = plugin.toggle()
    override suspend fun isOn() = plugin.getState()
}