package de.crysxd.octoapp.octoprint.plugins.mmu2filamentselect

import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface Mmu2FilamentSelectApi {

    @POST("plugin/mmu2filamentselect")
    suspend fun selectChoice(@Body body: Mmu2FilamentSelectChoice): Response<Unit>

    class Wrapper(private val api: Mmu2FilamentSelectApi) {
        suspend fun selectChoice(choice: Int) {
            api.selectChoice(Mmu2FilamentSelectChoice(choice))
        }
    }
}