package de.crysxd.octoapp.pluginsupport.octolapse

import android.graphics.PointF
import de.crysxd.octoapp.base.gcode.render.GcodeRenderView

sealed class OctolapseState {
    object Close : OctolapseState()
    data class Loading(val title: Int) : OctolapseState()
    data class SnapshotPlan(
        val layers: List<Layer>?,
        val jobId: String?
    ) : OctolapseState()

    data class Layer(
        val initialPosition: PointF?,
        val snapshotPosition: PointF?,
        val returnPosition: PointF?,
        val layerNumber: Int,
        val layerCount: Int,
        val renderParams: GcodeRenderView.RenderParams?,
    )
}
