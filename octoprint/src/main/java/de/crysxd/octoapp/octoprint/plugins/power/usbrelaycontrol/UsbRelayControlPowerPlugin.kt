package de.crysxd.octoapp.octoprint.plugins.power.usbrelaycontrol

import de.crysxd.octoapp.octoprint.models.settings.Settings
import de.crysxd.octoapp.octoprint.plugins.power.PowerPlugin

class UsbRelayControlPowerPlugin(
    private val api: UsbRelayControlApi
) : PowerPlugin<UsbRelayControlPowerDevice> {

    internal suspend fun turnOn(device: UsbRelayControlPowerDevice) {
        api.sendCommand(UsbRelayControlCommand.TurnUsbRelayOn(device))
    }

    internal suspend fun turnOff(device: UsbRelayControlPowerDevice) {
        api.sendCommand(UsbRelayControlCommand.TurnUsbRelayOff(device))
    }

    internal suspend fun isOn(device: UsbRelayControlPowerDevice) =
        api.getGpioState().getOrNull(device.index) == RelayState.ON

    override fun getDevices(settings: Settings) =
        settings.plugins.values.mapNotNull {
            it as? Settings.UsbRelaySettings
        }.firstOrNull()?.devices?.map {
            it.copy(plugin = this)
        } ?: emptyList()
}