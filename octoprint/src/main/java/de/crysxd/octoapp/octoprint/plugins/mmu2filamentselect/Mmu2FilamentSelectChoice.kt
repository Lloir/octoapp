package de.crysxd.octoapp.octoprint.plugins.mmu2filamentselect

data class Mmu2FilamentSelectChoice(
    val choice: Int,
    val command: String = "select",
)