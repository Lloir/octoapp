package de.crysxd.octoapp.base.network

import android.content.Context
import de.crysxd.octoapp.base.R
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import de.crysxd.octoapp.octoprint.exceptions.BasicAuthRequiredException
import de.crysxd.octoapp.octoprint.exceptions.InvalidApiKeyException
import de.crysxd.octoapp.octoprint.exceptions.OctoPrintException
import de.crysxd.octoapp.octoprint.exceptions.OctoPrintHttpsException
import de.crysxd.octoapp.octoprint.exceptions.RemoteServiceConnectionBrokenException
import de.crysxd.octoapp.octoprint.exceptions.WebSocketUpgradeFailedException
import kotlinx.coroutines.runBlocking
import okhttp3.Interceptor
import timber.log.Timber

class DetectBrokenSetupInterceptor(
    private val context: Context,
    private val octoPrintRepository: OctoPrintRepository,
    private val octoPrintId: String,
) : Interceptor {

    companion object {
        var enabled = true
    }

    override fun intercept(chain: Interceptor.Chain) = try {
        chain.proceed(chain.request())
    } catch (e: Throwable) {
        handleException(e)
        throw e
    }

    private fun isBrokenSetup(e: Throwable) =
        e is BasicAuthRequiredException || e is OctoPrintHttpsException || e is WebSocketUpgradeFailedException || e is InvalidApiKeyException || e is RemoteServiceConnectionBrokenException

    fun handleException(e: Throwable) {
        if (enabled && e is OctoPrintException && isBrokenSetup(e)) {
            val active = octoPrintRepository.getActiveInstanceSnapshot() ?: return
            val isForActive = octoPrintRepository.getActiveInstanceSnapshot()?.id == octoPrintId

            // Only handle if the OctoPrint having the issue is active
            if (!isForActive) {
                return
            }

            // Check navigation action
            val uri = when {
                e is RemoteServiceConnectionBrokenException -> null
                active.isForPrimaryWebUrl(e.webUrl) -> UriLibrary.getFixOctoPrintConnectionUri(baseUrl = active.webUrl, instanceId = active.id)
                else -> null
            }

            // Remove remote connection if broken
            if (e is RemoteServiceConnectionBrokenException) {
                runBlocking {
                    Timber.w("Caught OctoEverywhere/SpaghettiDetective/ngrok exception, removing connection")
                    octoPrintRepository.update(octoPrintId) {
                        it.copy(alternativeWebUrl = null, octoEverywhereConnection = null)
                    }
                }
            }

            // Dispatch to user
            ExceptionReceivers.dispatchException(
                BrokenSetupException(
                    original = e,
                    navigationAction = uri,
                    userMessage = when (e) {
                        is OctoPrintHttpsException -> context.getString(R.string.sign_in___broken_setup___https_issue, e.originalCause?.message ?: e.message)
                        is BasicAuthRequiredException -> context.getString(R.string.sign_in___broken_setup___basic_auth_required)
                        is InvalidApiKeyException -> context.getString(R.string.sign_in___broken_setup___api_key_revoked)
                        is WebSocketUpgradeFailedException -> context.getString(R.string.sign_in___broken_setup___websocket_upgrade_failed)
                        else -> e.userFacingMessage.replace("\n", "<br>")
                    }
                )
            )
        }
    }

    fun interface Factory {
        fun buildFor(octoPrintId: String): DetectBrokenSetupInterceptor
    }
}