package de.crysxd.octoapp.octoprint.json

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter

class SafeDoubleAdapter : TypeAdapter<Double>() {
    override fun write(out: JsonWriter, value: Double?) {
        out.value(value)
    }

    override fun read(reader: JsonReader): Double? = when (reader.peek()) {
        JsonToken.NUMBER -> reader.nextDouble()
        JsonToken.STRING -> reader.nextString().toDoubleOrNull()
        else -> {
            reader.skipValue()
            null
        }
    }
}