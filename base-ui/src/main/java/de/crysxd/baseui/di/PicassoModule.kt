package de.crysxd.baseui.di

import android.content.Context
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.map
import com.squareup.picasso.LruCache
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import de.crysxd.octoapp.base.logging.TimberLogger
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.octoprint.logging.LoggingInterceptorLogger
import de.crysxd.octoapp.octoprint.resolvePath
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import timber.log.Timber
import java.util.logging.Logger
import com.squareup.picasso.Cache as PicassoCache
import okhttp3.Cache as OkHttpCache

@Module
class PicassoModule {

    @Provides
    @BaseUiScope
    fun providePicassoCache(context: Context): PicassoCache = LruCache(context)

    @Provides
    @BaseUiScope
    fun providePublicPicasso(
        context: Context,
        okHttpCache: OkHttpCache,
        picassoCache: PicassoCache,
    ): Picasso = OkHttpClient.Builder()
        .cache(okHttpCache)
        .addNetworkInterceptor(
            HttpLoggingInterceptor(
                LoggingInterceptorLogger(TimberLogger(Logger.getLogger("Picasso/HTTP")).logger)
            ).setLevel(HttpLoggingInterceptor.Level.HEADERS)
        )
        .build()
        .let { okHttp ->
            Picasso.Builder(context)
                .memoryCache(picassoCache)
                .downloader(OkHttp3Downloader(okHttp))
                .build()
        }


    @Provides
    @BaseUiScope
    fun providePicasso(
        context: Context,
        octoPrintProvider: OctoPrintProvider,
        picassoCache: PicassoCache,
    ): LiveData<Picasso?> = octoPrintProvider.octoPrintFlow().asLiveData().map {
        it?.let { octoPrint ->
            Picasso.Builder(context)
                .downloader(OkHttp3Downloader(octoPrint.createOkHttpClient()))
                .memoryCache(picassoCache)
                .requestTransformer { request ->
                    if (request.uri.scheme == "file") {
                        request
                    } else {
                        request.uri?.let { uri ->
                            val newUri = octoPrint.webUrl
                                .resolvePath(uri.path)
                                .newBuilder()
                                .query(uri.query)
                                .build()

                            Timber.d("Mapping $uri -> $newUri")

                            request.buildUpon()
                                .setUri(Uri.parse(newUri.toString()))
                                .build()
                        } ?: request
                    }
                }.build()
        }
    }
}