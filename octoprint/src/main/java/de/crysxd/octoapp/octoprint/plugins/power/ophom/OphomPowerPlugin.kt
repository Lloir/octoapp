package de.crysxd.octoapp.octoprint.plugins.power.ophom

import de.crysxd.octoapp.octoprint.models.settings.Settings
import de.crysxd.octoapp.octoprint.plugins.power.PowerPlugin

class OphomPowerPlugin(
    private val ophomApi: OphomApi
) : PowerPlugin<OphomPowerDevice> {

    internal suspend fun toggle() = ophomApi.toggle()

    internal suspend fun getState() = ophomApi.getState().isOn

    override fun getDevices(settings: Settings) = settings.plugins.filterValues {
        it is Settings.OphomSettings
    }.map {
        OphomPowerDevice(this)
    }
}