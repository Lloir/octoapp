package de.crysxd.baseui.common.configureremote

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.transition.TransitionManager
import de.crysxd.baseui.R
import de.crysxd.baseui.databinding.ConfigureRemoteAccessOctoeverywhereFragmentBinding
import de.crysxd.baseui.di.injectParentViewModel
import de.crysxd.baseui.utils.InstantAutoTransition
import de.crysxd.octoapp.octoprint.isOctoEverywhereUrl

class ConfigureRemoteAccessOctoEverywhereFragment : Fragment() {

    private val viewModel by injectParentViewModel<ConfigureRemoteAccessViewModel>()
    private lateinit var binding: ConfigureRemoteAccessOctoeverywhereFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        ConfigureRemoteAccessOctoeverywhereFragmentBinding.inflate(inflater, container, false).also { binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.connectOctoEverywhere.setOnClickListener {
            viewModel.getOctoEverywhereAppPortalUrl()
        }

        binding.disconnectOctoEverywhere.setOnClickListener {
            viewModel.setRemoteUrl("", "", "", false)
        }

        viewModel.viewState.observe(viewLifecycleOwner) {
            TransitionManager.beginDelayedTransition(binding.root, InstantAutoTransition())
            binding.connectOctoEverywhere.isEnabled = it !is ConfigureRemoteAccessViewModel.ViewState.Loading
            binding.connectOctoEverywhere.text = getString(
                when (it) {
                    ConfigureRemoteAccessViewModel.ViewState.Idle -> R.string.configure_remote_acces___octoeverywhere___connect_button
                    ConfigureRemoteAccessViewModel.ViewState.Loading -> R.string.loading
                }
            )
        }

        viewModel.viewData.observe(viewLifecycleOwner) {
            val oeConnected = it.remoteWebUrl != null && it.remoteWebUrl.isOctoEverywhereUrl()
            binding.octoEverywhereConnected.isVisible = oeConnected
            binding.disconnectOctoEverywhere.isVisible = oeConnected
            binding.connectOctoEverywhere.isVisible = !oeConnected
            binding.description1.isVisible = !oeConnected
            binding.usps.isVisible = !oeConnected
        }

        createUsps()
    }

    private fun createUsps() {
        binding.usps.configure(
            iconColorInt = ContextCompat.getColor(requireContext(), R.color.white_translucent),
            textColorInt = ContextCompat.getColor(requireContext(), R.color.white),
            backgroundColorInt = ContextCompat.getColor(requireContext(), R.color.white_translucent_3),
        )
        binding.usps.addUsp(
            iconRes = R.drawable.ic_round_network_check_24,
            description = getString(R.string.configure_remote_acces___octoeverywhere___usp_1)
        )
        binding.usps.addUsp(
            iconRes = R.drawable.ic_round_videocam_24,
            description = getString(R.string.configure_remote_acces___octoeverywhere___usp_2)
        )
        binding.usps.addUsp(
            iconRes = R.drawable.ic_round_star_24,
            description = getString(R.string.configure_remote_acces___octoeverywhere___usp_3)
        )
        binding.usps.addUsp(
            iconRes = R.drawable.ic_round_lock_24,
            description = getString(R.string.configure_remote_acces___octoeverywhere___usp_4)
        )
    }
}