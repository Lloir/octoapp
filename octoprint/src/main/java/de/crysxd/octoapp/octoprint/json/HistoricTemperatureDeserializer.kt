package de.crysxd.octoapp.octoprint.json

import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import de.crysxd.octoapp.octoprint.models.printer.PrinterState
import de.crysxd.octoapp.octoprint.models.socket.HistoricTemperatureData
import java.lang.reflect.Type

class HistoricTemperatureDeserializer(private val gson: Gson) : JsonDeserializer<HistoricTemperatureData> {

    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): HistoricTemperatureData {
        val time = json.asJsonObject["time"].asLong
        val components = json.asJsonObject.keySet().filter {
            it != "time"
        }.associateWith {
            gson.fromJson(json.asJsonObject[it], PrinterState.ComponentTemperature::class.java)
        }

        return HistoricTemperatureData(
            time = time,
            components = components
        )
    }
}