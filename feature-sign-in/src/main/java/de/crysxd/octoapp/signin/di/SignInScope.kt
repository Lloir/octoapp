package de.crysxd.octoapp.signin.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class SignInScope