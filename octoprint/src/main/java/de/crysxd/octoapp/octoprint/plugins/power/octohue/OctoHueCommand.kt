package de.crysxd.octoapp.octoprint.plugins.power.octohue

data class OctoHueCommand(val command: String = "togglehue")
