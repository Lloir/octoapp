package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.octoprint.models.files.FileCommand
import de.crysxd.octoapp.octoprint.models.files.FileObject
import de.crysxd.octoapp.octoprint.plugins.pluginmanager.PLUGIN_MMU2_FILAMENT_SELECT
import timber.log.Timber
import javax.inject.Inject

class StartPrintJobUseCase @Inject constructor(
    private val octoPrintProvider: OctoPrintProvider,
    private val octoPrintRepository: OctoPrintRepository,
    private val octoPreferences: OctoPreferences,
) : UseCase<StartPrintJobUseCase.Params, StartPrintJobUseCase.Result>() {

    override suspend fun doExecute(param: Params, timber: Timber.Tree): Result {
        val octoprint = octoPrintProvider.octoPrint()
        val instance = octoPrintRepository.getActiveInstanceSnapshot()
        val settings = instance?.settings ?: octoprint.createSettingsApi().getSettings()
        val materialManagerAvailable = octoprint.createMaterialManagerPluginsCollection().isMaterialManagerAvailable(settings)
        val timelapseConfigRequired = octoPreferences.askForTimelapseBeforePrinting

        // If a material manager is present and the selection was not confirmed, we need material selection
        // If the MMU2 plugin is installed, we skip this step as the MMU2 plugin will ask for the material
        if (materialManagerAvailable && !param.materialSelectionConfirmed && instance?.hasPlugin(PLUGIN_MMU2_FILAMENT_SELECT) != true) {
            return Result.MaterialSelectionRequired
        }

        if (timelapseConfigRequired && !param.timelapseConfigConfirmed) {
            return Result.TimelapseConfigRequired
        }

        OctoAnalytics.logEvent(OctoAnalytics.Event.PrintStartedByApp)
        octoPrintProvider.octoPrint().createFilesApi().executeFileCommand(param.file, FileCommand.SelectFile(true))
        return Result.PrintStarted
    }

    data class Params(
        val file: FileObject.File,
        val materialSelectionConfirmed: Boolean,
        val timelapseConfigConfirmed: Boolean,
    )

    sealed class Result {
        object PrintStarted : Result()
        object MaterialSelectionRequired : Result()
        object TimelapseConfigRequired : Result()
    }
}