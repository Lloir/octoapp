package de.crysxd.octoapp.octoprint.plugins.power.enclosure

import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PATCH
import retrofit2.http.Path

interface EnclosureOutputApi {

    @PATCH("plugin/enclosure/outputs/{indexId}")
    suspend fun sendCommand(
        @Path("indexId") indexId: Int,
        @Body command: EnclosureOutputCommand
    ): Response<Unit>

    @GET("plugin/enclosure/outputs/{indexId}")
    suspend fun getGpioState(
        @Path("indexId") indexId: Int
    ): EnclosureOutputResponse

}