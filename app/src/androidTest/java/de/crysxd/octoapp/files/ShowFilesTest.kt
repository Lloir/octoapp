package de.crysxd.octoapp.files

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.pressBack
import androidx.test.espresso.action.ViewActions.swipeUp
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.isRoot
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.adevinta.android.barista.rule.BaristaRule
import com.adevinta.android.barista.rule.flaky.AllowFlaky
import de.crysxd.octoapp.MainActivity
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.framework.TestEnvironmentLibrary
import de.crysxd.octoapp.framework.WorkspaceRobot
import de.crysxd.octoapp.framework.ext.setActive
import de.crysxd.octoapp.framework.rules.AutoConnectPrinterRule
import de.crysxd.octoapp.framework.rules.IdleTestEnvironmentRule
import de.crysxd.octoapp.framework.rules.ResetDaggerRule
import de.crysxd.octoapp.framework.rules.TestDocumentationRule
import de.crysxd.octoapp.framework.waitForNot
import org.hamcrest.Matchers.allOf
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain

class ShowFilesTest {

    private val testEnv = TestEnvironmentLibrary.Terrier
    private val baristaRule = BaristaRule.create(MainActivity::class.java)

    @get:Rule
    val chain = RuleChain.outerRule(baristaRule)
        .around(IdleTestEnvironmentRule(testEnv))
        .around(TestDocumentationRule())
        .around(ResetDaggerRule())
        .around(AutoConnectPrinterRule())

    @Before
    fun setUp() {
        BaseInjector.get().octorPrintRepository().setActive(testEnv)
    }

    @After
    fun tearDown() {
        BillingManager.enabledForTest = null
    }

    @Test(timeout = 60_000)
    @AllowFlaky(attempts = 3)
    fun WHEN_files_are_opened_THEN_files_are_listed() {
        // GIVEN
        BillingManager.enabledForTest = true
        baristaRule.launchActivity()

        // Open files
        WorkspaceRobot.waitForPrepareWorkspace()
        onView(withText(R.string.start_printing)).perform(click())

        // Check slicer hint is shown and can be dismissed
        onView(withText(R.string.file_manager___thumbnail_info___title)).check(matches(isDisplayed()))
        onView(withText(R.string.file_manager___thumbnail_info___details)).check(matches(isDisplayed()))
        onView(withText(R.string.hide)).perform(click())
        waitForNot(allOf(withText(R.string.hide), isDisplayed()))

        // Check files listed
        onView(withText(R.string.file_manager___file_list___your_files)).check(matches(isDisplayed()))
        onView(withId(R.id.recyclerViewFileList)).perform(swipeUp())
        onView(withText("layers.gcode")).check(matches(isDisplayed()))
        onView(withText("test-ü-ä-ö.gcode")).check(matches(isDisplayed()))
        onView(withText("test-ñ.gcode")).check(matches(isDisplayed()))
        onView(withText("test-#-ü-ñ--% \uD83E\uDD72")).check(matches(isDisplayed()))
        onView(withText("test-\uD83D\uDE0E \uD83E\uDD2F \uD83D\uDE0D.gcode")).check(matches(isDisplayed()))
        onView(withText("test-ñ.gcode")).check(matches(isDisplayed()))
        onView(withText("test-#-?-%.gcode")).perform(click())

        // Check tabs
        onView(withText(R.string.file_manager___file_details___tab_preview)).check(matches(isDisplayed()))
        onView(withText(R.string.file_manager___file_details___tab_info)).check(matches(isDisplayed()))

        // Check some details
        onView(withText("test-#-?-%.gcode")).check(matches(isDisplayed()))
        onView(withText(R.string.file_manager___file_details___print_time)).check(matches(isDisplayed()))
        onView(withText(R.string.file_manager___file_details___model_size)).check(matches(isDisplayed()))
        onView(withText(R.string.file_manager___file_details___filament_use)).check(matches(isDisplayed()))
        onView(withText(R.string.location)).check(matches(isDisplayed()))
        onView(withText(R.string.file_manager___file_details___path)).check(matches(isDisplayed()))
        onView(withText(R.string.file_manager___file_details___file_size)).check(matches(isDisplayed()))
        onView(withText(R.string.file_manager___file_details___uploaded)).check(matches(isDisplayed()))
        onView(withText(R.string.file_manager___file_details___last_print)).check(matches(isDisplayed()))

        // Check Gcode
//        onView(withText(R.string.file_manager___file_details___tab_preview)).perform(click())
//        waitFor(allOf(withText("1 of 198"), isDisplayed()), 10_000)
        onView(isRoot()).perform(pressBack())

        // Open folder and check special file
        onView(withText("test-#-ü-ñ--% \uD83E\uDD72")).perform(click())
        onView(withText("test-#-ü-ñ--% \uD83E\uDD72")).check(matches(isDisplayed()))
        onView(withText("test-\uD83D\uDE0E \uD83E\uDD2F \uD83D\uDE0D-ü-ñ--#-%-@.gcode")).perform(click())
        onView(withText("test-\uD83D\uDE0E \uD83E\uDD2F \uD83D\uDE0D-ü-ñ--#-%-@.gcode")).check(matches(isDisplayed()))
        onView(withText(R.string.file_manager___file_details___path)).check(matches(isDisplayed()))
//        onView(withText(R.string.file_manager___file_details___tab_preview)).perform(click())
//        waitFor(allOf(withText("1 of 198"), isDisplayed()), 10_000)
        onView(isRoot()).perform(pressBack())
    }
}