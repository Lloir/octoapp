package de.crysxd.octoapp.octoprint.plugins.power.octohue

import de.crysxd.octoapp.octoprint.models.settings.Settings
import de.crysxd.octoapp.octoprint.plugins.power.PowerPlugin

class OctoHuePowerPlugin(
    private val octoHueApi: OctoHueApi
) : PowerPlugin<OctoHuePowerDevice> {

    internal suspend fun toggle() {
        octoHueApi.toggle()
    }

    override fun getDevices(settings: Settings) = settings.plugins.filterValues {
        it is Settings.OctoHueSettings
    }.map {
        OctoHuePowerDevice(this)
    }
}