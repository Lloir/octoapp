package de.crysxd.octoapp.octoprint.plugins.power.ocotrelay

data class OctoRelayResponse(
    val status: Boolean?
)