package de.crysxd.octoapp.base.data.models

enum class AppTheme {
    AUTO, LIGHT, DARK
}