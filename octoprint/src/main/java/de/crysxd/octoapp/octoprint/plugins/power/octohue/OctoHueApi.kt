package de.crysxd.octoapp.octoprint.plugins.power.octohue

import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface OctoHueApi {

    @POST("plugin/octohue")
    suspend fun toggle(@Body command: OctoHueCommand = OctoHueCommand()): Response<Unit?>

}