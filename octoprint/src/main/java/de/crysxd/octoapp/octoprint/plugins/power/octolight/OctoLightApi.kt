package de.crysxd.octoapp.octoprint.plugins.power.octolight

import retrofit2.http.GET

interface OctoLightApi {

    @GET("plugin/octolight?action=toggle")
    suspend fun toggleLight()

    @GET("plugin/octolight?action=turnOn")
    suspend fun turnOn()

    @GET("plugin/octolight?action=turnOff")
    suspend fun turnOff()

    @GET("plugin/octolight?action=getState")
    suspend fun getState(): OctoLightState

}