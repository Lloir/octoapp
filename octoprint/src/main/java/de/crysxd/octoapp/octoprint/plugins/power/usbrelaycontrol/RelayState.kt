package de.crysxd.octoapp.octoprint.plugins.power.usbrelaycontrol

import com.google.gson.annotations.SerializedName

enum class RelayState {
    @SerializedName("on")
    ON,

    @SerializedName("off")
    OFF
}