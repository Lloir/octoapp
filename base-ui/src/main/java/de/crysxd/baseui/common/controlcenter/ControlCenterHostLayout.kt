package de.crysxd.baseui.common.controlcenter

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.PointF
import android.util.AttributeSet
import android.util.TypedValue
import android.view.MotionEvent
import android.view.animation.DecelerateInterpolator
import android.widget.FrameLayout
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import timber.log.Timber
import kotlin.math.absoluteValue


@SuppressLint("ClickableViewAccessibility")
class ControlCenterHostLayout @JvmOverloads constructor(context: Context, attributeSet: AttributeSet? = null) : FrameLayout(context, attributeSet) {

    companion object {
        private const val MAX_DRAG_DISTANCE = 0.25f
        private const val MIN_DRAG_DISTANCE = 0.05f
        private const val MIN_FLING_VELOCITY = 1.2f
    }

    private val disableLifecycles = mutableListOf<Lifecycle>()
    private val controlCenterView by lazy { getChildAt(0) }
    lateinit var controlCenterFragment: Fragment
    var fragmentManager: FragmentManager? = null
        set(value) {
            field = value
            controlCenterView.isVisible = false
            controlCenterView.translationZ = 100f
            controlCenterView.setOnTouchListener { _, _ -> true }
            value?.beginTransaction()
                ?.add(controlCenterView.id, controlCenterFragment)
                ?.detach(controlCenterFragment)
                ?.commitAllowingStateLoss()
        }

    private var applyTranslation = true
    private var dragStartPoint = PointF()
    private var dragEndPoint = PointF()
    private var dragStartProgress = 0f
    private var dragStartTime = 0L
    private var dragProgress = 0f
        set(value) {
            field = value
            controlCenterView.alpha = dragProgress

            val wasVisible = controlCenterView.isVisible
            val isVisible = dragProgress > 0
            controlCenterView.isVisible = isVisible
            synchronizeViewTranslationWithTouch()

            if (isVisible != wasVisible) {
                fragmentManager?.beginTransaction()?.let {
                    if (isVisible) it.attach(controlCenterFragment) else it.detach(controlCenterFragment)
                }?.commit()
            }
        }

    fun disableForLifecycle(lifecycle: Lifecycle) {
        disableLifecycles.add(lifecycle)
        lifecycle.addObserver(object : DefaultLifecycleObserver {
            override fun onPause(owner: LifecycleOwner) {
                super.onPause(owner)
                updateEnabled()
            }

            override fun onResume(owner: LifecycleOwner) {
                super.onResume(owner)
                updateEnabled()
            }

            override fun onDestroy(owner: LifecycleOwner) {
                super.onDestroy(owner)
                disableLifecycles.remove(lifecycle)
                lifecycle.removeObserver(this)
            }
        })
    }

    private fun updateEnabled() {
        isEnabled = disableLifecycles.all { it.currentState < Lifecycle.State.RESUMED }
    }

    override fun onInterceptTouchEvent(ev: MotionEvent) = when {
        isEnabled && ev.action == MotionEvent.ACTION_MOVE && startedAwayFromScreenEdge() -> {
            controlCenterFragment
            ev.isAccepted()
        }

        isEnabled && ev.action == MotionEvent.ACTION_DOWN -> {
            dragStartPoint.x = ev.x
            dragStartPoint.y = ev.y
            dragStartTime = System.currentTimeMillis()
            dragStartProgress = if (controlCenterView.isVisible) 1f else 0f
            false
        }

        else -> {
            if (startedAwayFromScreenEdge()) {
                when {
                    ev.isFling() -> completeDrag()
                    ev.isAccepted() -> rejectDrag()
                }
            }
            super.onInterceptTouchEvent(ev)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(ev: MotionEvent): Boolean = when {
        isEnabled && ev.action == MotionEvent.ACTION_DOWN -> {
            // We need this if the current screen does not have any touch listeners at the touched position.
            // In this case we need to act as a touch listener for down to start receiving move events
            startedAwayFromScreenEdge()
        }

        isEnabled && ev.action == MotionEvent.ACTION_MOVE && startedAwayFromScreenEdge() -> {
            applyTranslation = true
            dragEndPoint.x = ev.x
            dragEndPoint.y = ev.y
            dragProgress = if (ev.isAccepted()) ev.dragProgress() else dragStartProgress
            true
        }

        isEnabled && ev.action == MotionEvent.ACTION_UP -> {
            when {
                ev.isFling() -> completeDrag()
                dragProgress != 1f && dragProgress != 0f -> rejectDrag()
            }
            true
        }

        else -> false
    }

    private fun startedAwayFromScreenEdge(): Boolean {
        val minDistanceHorizontal = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40f, resources.displayMetrics)
        val minDistanceVertical = minDistanceHorizontal * 2
        val awayFromLeft = dragStartPoint.x > minDistanceHorizontal
        val awayFromRight = (width - dragStartPoint.x) > minDistanceHorizontal
        val awayFromBottom = (height - dragStartPoint.y) > minDistanceVertical
        val awayFromTop = dragStartPoint.y > minDistanceVertical
        Timber.i("Started away from edges: awayFromBottom=$awayFromBottom awayFromTop=$awayFromTop awayFromLeft=$awayFromLeft awayFromRight=$awayFromRight")
        return awayFromBottom && awayFromTop && awayFromLeft && awayFromRight
    }

    private fun MotionEvent.isAccepted(): Boolean {
        // We accept this event if we move primarily sideways and if we moved the min distance
        val distanceX = (x - dragStartPoint.x).absoluteValue
        val distanceY = (y - dragStartPoint.y).absoluteValue
        val minXDistance = width * MIN_DRAG_DISTANCE
        return distanceX > distanceY * 3 && distanceX > minXDistance
    }

    private fun MotionEvent.isFling(): Boolean {
        val distanceX = (x - dragStartPoint.x).absoluteValue
        val distanceY = (y - dragStartPoint.y).absoluteValue
        val isXFling = distanceX > distanceY * 2
        val velocityX = distanceX / (System.currentTimeMillis() - dragStartTime)
        return velocityX >= MIN_FLING_VELOCITY && isXFling
    }

    private fun MotionEvent.dragProgress(): Float {
        val distanceX = (x - dragStartPoint.x).absoluteValue
        val minXDistance = width * MIN_DRAG_DISTANCE
        val maxXDistance = width * MAX_DRAG_DISTANCE
        val progress = ((distanceX - minXDistance) / maxXDistance).coerceIn(0f, 1f)
        return if (dragStartProgress == 1f) 1 - progress else progress
    }

    private fun synchronizeViewTranslationWithTouch() {
        controlCenterFragment.view?.translationX = if (applyTranslation) {
            val minXDistance = width * MIN_DRAG_DISTANCE
            val maxXDistance = width * MAX_DRAG_DISTANCE
            val direction1 = if (dragEndPoint.x > dragStartPoint.x) -1f else 1f
            val direction2 = if (dragStartProgress == 1f) -1f else 1f
            (maxXDistance - minXDistance) * (1 - dragProgress) * direction1 * direction2
        } else {
            0f
        }
    }

    fun dismiss() {
        dragStartProgress = 0f
        applyTranslation = false
        rejectDrag()
    }

    fun open() {
        dragStartProgress = 1f
        applyTranslation = false
        rejectDrag()
    }

    private fun completeDrag() = animateToProgress(1 - dragStartProgress)

    private fun rejectDrag() = animateToProgress(dragStartProgress)

    private fun animateToProgress(progress: Float) {
        ValueAnimator.ofFloat(dragProgress, progress).also {
            it.addUpdateListener {
                dragProgress = it.animatedValue as Float
            }
            it.duration = 250
            it.interpolator = DecelerateInterpolator()
        }.start()
    }
}
