package de.crysxd.octoapp.octoprint.plugins.power.enclosure

import com.google.gson.annotations.SerializedName

data class EnclosureOutputResponse(
    @SerializedName("current_value") val currentValue: Boolean?
)
