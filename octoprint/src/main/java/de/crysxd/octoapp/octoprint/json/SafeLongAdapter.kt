package de.crysxd.octoapp.octoprint.json

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter

class SafeLongAdapter : TypeAdapter<Long>() {
    override fun write(out: JsonWriter, value: Long?) {
        out.value(value)
    }

    override fun read(reader: JsonReader): Long? = when (reader.peek()) {
        JsonToken.NUMBER -> reader.nextLong()
        JsonToken.STRING -> reader.nextString().toLongOrNull()
        else -> {
            reader.skipValue()
            null
        }
    }
}