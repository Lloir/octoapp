package de.crysxd.baseui.menu.settings

import android.content.Context
import android.os.Build
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import de.crysxd.baseui.R
import de.crysxd.baseui.common.LinkClickMovementMethod
import de.crysxd.baseui.menu.base.Menu
import de.crysxd.baseui.menu.base.MenuHost
import de.crysxd.baseui.menu.base.MenuItem
import de.crysxd.baseui.menu.base.MenuItemStyle
import de.crysxd.baseui.menu.base.RevolvingOptionsMenuItem
import de.crysxd.baseui.menu.base.SubMenuItem
import de.crysxd.baseui.menu.base.ToggleMenuItem
import de.crysxd.baseui.menu.switchprinter.SwitchOctoPrintMenu
import de.crysxd.baseui.widget.WidgetHostFragment
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.data.models.AppTheme
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_AUTOMATIC_LIGHTS
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_AUTO_CONNECT_PRINTER
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_CACHES_MENU
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_CHANGE_LANGUAGE
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_CONFIRM_POWER_OFF
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_CUSTOMIZE_WIDGETS
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_HELP
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_LEFT_HAND_MODE
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_LIVE_NOTIFICATION
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_NIGHT_THEME
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_RESET_DEFAULT_POWER_DEVICES
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_SCREEN_ON_DURING_PRINT
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_SHOW_CHANGE_OCTOPRINT_MENU
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.open
import de.crysxd.octoapp.base.usecase.SetAppLanguageUseCase
import kotlinx.coroutines.runBlocking
import kotlinx.parcelize.Parcelize

@Parcelize
class SettingsMenu : Menu {
    override suspend fun getMenuItem() = listOf(
        HelpMenuItem(),
        ChangeLanguageMenuItem(),
        AppThemeMenuItem(),
        PrintNotificationMenuItem(),
        KeepScreenOnDuringPrintMenuItem(),
        AutoConnectPrinterMenuItem(),
        ChangeOctoPrintInstanceMenuItem(),
        CustomizeWidgetsMenuItem(),
        ShowOctoAppLabMenuItem(),
        AutomaticLightsSettingsMenuItem(),
        ConfirmPowerOffSettingsMenuItem(),
        LeftHandModeMenuItem(),
        ResetDefaultPowerDevicesMenuItem(),
        CachesMenuItem(),
    )

    override suspend fun getTitle(context: Context) = context.getString(R.string.main_menu___menu_settings_title)
    override suspend fun getSubtitle(context: Context) = context.getString(R.string.main_menu___submenu_subtitle)
    override fun getBottomText(context: Context) = HtmlCompat.fromHtml(
        context.getString(
            R.string.main_menu___about_text,
            ContextCompat.getColor(context, R.color.dark_text),
            context.packageManager.getPackageInfo(context.packageName, 0).versionName
        ),
        HtmlCompat.FROM_HTML_MODE_COMPACT
    )

    override fun getBottomMovementMethod(host: MenuHost) =
        LinkClickMovementMethod(object : LinkClickMovementMethod.OpenWithIntentLinkClickedListener(host.getMenuActivity()) {
            override fun onLinkClicked(context: Context, url: String?): Boolean {
                return if (url == "privacy") {
                    host.pushMenu(PrivacyMenu())
                    true
                } else {
                    super.onLinkClicked(context, url)
                }
            }
        })
}

class HelpMenuItem : MenuItem {
    override val itemId = MENU_ITEM_HELP
    override var groupId = ""
    override val order = 101
    override val enforceSingleLine = false
    override val style = MenuItemStyle.Settings
    override val icon = R.drawable.ic_round_help_outline_24

    override fun getTitle(context: Context) = context.getString(R.string.main_menu___item_help_faq_and_feedback)
    override suspend fun onClicked(host: MenuHost?) {
        host?.getMenuActivity()?.let {
            UriLibrary.getHelpUri().open(it)
        }
        host?.closeMenu()
    }
}

class ChangeLanguageMenuItem : MenuItem {
    override val itemId = MENU_ITEM_CHANGE_LANGUAGE
    override var groupId = ""
    override val order = 102
    override val enforceSingleLine = false
    override val style = MenuItemStyle.Settings
    override val icon = R.drawable.ic_round_translate_24

    override fun isVisible(destinationId: Int) = runBlocking {
        BaseInjector.get().getAppLanguageUseCase().execute(Unit).canSwitchLocale
    }

    override fun getTitle(context: Context) = runBlocking {
        BaseInjector.get().getAppLanguageUseCase().execute(Unit).switchLanguageText ?: ""
    }

    override suspend fun onClicked(host: MenuHost?) {
        val newLocale = BaseInjector.get().getAppLanguageUseCase().execute(Unit).switchLanguageLocale
        host?.getMenuActivity()?.let {
            BaseInjector.get().setAppLanguageUseCase().execute(SetAppLanguageUseCase.Param(newLocale, it))
        }
    }
}

class CustomizeWidgetsMenuItem : MenuItem {
    override val itemId = MENU_ITEM_CUSTOMIZE_WIDGETS
    override var groupId = ""
    override val order = 103
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = R.drawable.ic_round_person_pin_24
    override val canRunWithAppInBackground = false

    override fun isVisible(destinationId: Int) = destinationId == R.id.workspacePrePrint || destinationId == R.id.workspacePrint
    override fun getTitle(context: Context) = context.getString(R.string.main_menu___item_customize_widgets)
    override suspend fun onClicked(host: MenuHost?) {
        (host?.getHostFragment() as? WidgetHostFragment)?.startEdit()
        host?.closeMenu()
    }
}

class ResetDefaultPowerDevicesMenuItem : MenuItem {
    override val itemId = MENU_ITEM_RESET_DEFAULT_POWER_DEVICES
    override var groupId = ""
    override val order = 104
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = R.drawable.ic_round_power_24

    override fun getTitle(context: Context) = context.getString(R.string.main_menu___item_reset_default_power_devices)
    override suspend fun onClicked(host: MenuHost?) {
        BaseInjector.get().octorPrintRepository().updateActive {
            it.copy(appSettings = it.appSettings?.copy(defaultPowerDevices = null))
        }
    }
}

class AppThemeMenuItem : RevolvingOptionsMenuItem() {
    private val context = BaseInjector.get().localizedContext()
    override val activeValue get() = BaseInjector.get().octoPreferences().appTheme.name
    override val options = listOfNotNull(
        Option(context.getString(R.string.main_menu___item_app_theme_auto), AppTheme.AUTO.toString()).takeIf { Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q },
        Option(context.getString(R.string.main_menu___item_app_theme_dark), AppTheme.DARK.toString()),
        Option(context.getString(R.string.main_menu___item_app_theme_light), AppTheme.LIGHT.toString()),
    )
    override val itemId = MENU_ITEM_NIGHT_THEME
    override var groupId = ""
    override val order = 105
    override val enforceSingleLine = false
    override val style = MenuItemStyle.Settings
    override val icon = R.drawable.ic_round_dark_mode_24

    override fun getTitle(context: Context) = context.getString(R.string.main_menu___item_app_theme)

    override suspend fun handleOptionActivated(host: MenuHost?, option: Option) {
        BaseInjector.get().octoPreferences().appTheme = AppTheme.valueOf(option.value)
    }
}

class KeepScreenOnDuringPrintMenuItem : ToggleMenuItem() {
    override val isChecked get() = BaseInjector.get().octoPreferences().isKeepScreenOnDuringPrint
    override val itemId = MENU_ITEM_SCREEN_ON_DURING_PRINT
    override var groupId = ""
    override val order = 106
    override val style = MenuItemStyle.Settings
    override val icon = R.drawable.ic_round_brightness_high_24

    override fun getTitle(context: Context) = context.getString(R.string.main_menu___item_keep_screen_on_during_pinrt_on)
    override suspend fun handleToggleFlipped(host: MenuHost, enabled: Boolean) {
        BaseInjector.get().octoPreferences().isKeepScreenOnDuringPrint = enabled
    }
}

class AutoConnectPrinterMenuItem : ToggleMenuItem() {
    override val isChecked get() = BaseInjector.get().octoPreferences().isAutoConnectPrinter
    override val itemId = MENU_ITEM_AUTO_CONNECT_PRINTER
    override var groupId = ""
    override val order = 107
    override val style = MenuItemStyle.Settings
    override val icon = R.drawable.ic_round_hdr_auto_24px

    override fun getTitle(context: Context) = context.getString(R.string.main_menu___item_auto_connect_printer)
    override suspend fun handleToggleFlipped(host: MenuHost, enabled: Boolean) {
        BaseInjector.get().octoPreferences().isAutoConnectPrinter = enabled
    }
}

class LeftHandModeMenuItem : ToggleMenuItem() {
    override val isChecked get() = BaseInjector.get().octoPreferences().leftHandMode
    override val itemId = MENU_ITEM_LEFT_HAND_MODE
    override var groupId = ""
    override val order = 108
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = R.drawable.ic_round_left_back_hand_24

    override fun getTitle(context: Context) = context.getString(R.string.main_menu___item_left_hand_mode)
    override suspend fun handleToggleFlipped(host: MenuHost, enabled: Boolean) {
        BaseInjector.get().octoPreferences().leftHandMode = enabled
    }
}

class PrintNotificationMenuItem : SubMenuItem() {
    override val subMenu: Menu get() = PrintNotificationsMenu()
    override val itemId = MENU_ITEM_LIVE_NOTIFICATION
    override var groupId = ""
    override val order = 117
    override val enforceSingleLine = false
    override val style = MenuItemStyle.Settings
    override val icon = R.drawable.ic_round_notifications_active_24
    override fun getTitle(context: Context) = context.getString(R.string.main_menu___item_print_notifications)
}

class AutomaticLightsSettingsMenuItem : SubMenuItem() {
    override val itemId = MENU_ITEM_AUTOMATIC_LIGHTS
    override var groupId = ""
    override val order = 118
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = R.drawable.ic_round_wb_incandescent_24
    override val subMenu: Menu get() = AutomaticLightsSettingsMenu()

    override fun getTitle(context: Context) = context.getString(R.string.main_menu___item_automatic_lights)
}

class ConfirmPowerOffSettingsMenuItem : SubMenuItem() {
    override val itemId = MENU_ITEM_CONFIRM_POWER_OFF
    override var groupId = ""
    override val order = 119
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = R.drawable.ic_round_power_24
    override val subMenu: Menu get() = ConfirmPowerOffSettingsMenu()

    override fun getTitle(context: Context) = context.getString(R.string.main_menu___item_confirm_power_off)
}

open class CachesMenuItem : SubMenuItem() {
    override val itemId = MENU_ITEM_CACHES_MENU
    override var groupId = ""
    override val order = 129
    override val style: MenuItemStyle = MenuItemStyle.Settings
    override val icon = R.drawable.ic_round_sd_storage_24
    override val subMenu: Menu get() = CachesMenu()

    override fun getTitle(context: Context) = context.getString(R.string.caches_menu___title)
}

class ShowOctoAppLabMenuItem : SubMenuItem() {
    override val itemId = MENU_ITEM_SHOW_CHANGE_OCTOPRINT_MENU
    override var groupId = ""
    override val order = 130
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = R.drawable.ic_round_science_24px
    override val subMenu: Menu get() = OctoAppLabMenu()

    override fun getTitle(context: Context) = context.getString(R.string.main_menu___show_octoapp_lab)
}


open class ChangeOctoPrintInstanceMenuItem : SubMenuItem() {
    override val itemId = MENU_ITEM_SHOW_CHANGE_OCTOPRINT_MENU
    override var groupId = "change"
    override val order = 151
    override val style: MenuItemStyle = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = R.drawable.ic_round_swap_horiz_24
    override val subMenu: Menu get() = SwitchOctoPrintMenu()
    override fun isVisible(destinationId: Int) = !BillingManager.isFeatureEnabled(BillingManager.FEATURE_QUICK_SWITCH)

    override fun getTitle(context: Context) =
        context.getString(
            if (BaseInjector.get().octorPrintRepository().getAll().size > 1) {
                R.string.main_menu___item_change_octoprint_instance
            } else {
                R.string.main_menu___item_add_octoprint_instance
            }
        )
}