package de.crysxd.octoapp.octoprint

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.TypeAdapter
import com.google.gson.TypeAdapterFactory
import com.google.gson.reflect.TypeToken
import de.crysxd.octoapp.octoprint.api.ConnectionApi
import de.crysxd.octoapp.octoprint.api.FilesApi
import de.crysxd.octoapp.octoprint.api.JobApi
import de.crysxd.octoapp.octoprint.api.LoginApi
import de.crysxd.octoapp.octoprint.api.PrinterApi
import de.crysxd.octoapp.octoprint.api.PrinterProfileApi
import de.crysxd.octoapp.octoprint.api.ProbeApi
import de.crysxd.octoapp.octoprint.api.SettingsApi
import de.crysxd.octoapp.octoprint.api.SystemApi
import de.crysxd.octoapp.octoprint.api.TimelapseApi
import de.crysxd.octoapp.octoprint.api.UserApi
import de.crysxd.octoapp.octoprint.api.VersionApi
import de.crysxd.octoapp.octoprint.ext.withHostnameVerifier
import de.crysxd.octoapp.octoprint.ext.withSslKeystore
import de.crysxd.octoapp.octoprint.interceptors.AlternativeWebUrlInterceptor
import de.crysxd.octoapp.octoprint.interceptors.ApiKeyInterceptor
import de.crysxd.octoapp.octoprint.interceptors.BasicAuthInterceptor
import de.crysxd.octoapp.octoprint.interceptors.CatchAllInterceptor
import de.crysxd.octoapp.octoprint.interceptors.GenerateExceptionInterceptor
import de.crysxd.octoapp.octoprint.json.ConnectionStateDeserializer
import de.crysxd.octoapp.octoprint.json.FileObjectDeserializer
import de.crysxd.octoapp.octoprint.json.HistoricTemperatureDeserializer
import de.crysxd.octoapp.octoprint.json.MessageDeserializer
import de.crysxd.octoapp.octoprint.json.PluginSettingsDeserializer
import de.crysxd.octoapp.octoprint.json.ProgressInformationDeserializer
import de.crysxd.octoapp.octoprint.json.SafeDoubleAdapter
import de.crysxd.octoapp.octoprint.json.SafeFloatAdapter
import de.crysxd.octoapp.octoprint.json.SafeIntAdapter
import de.crysxd.octoapp.octoprint.json.SafeLongAdapter
import de.crysxd.octoapp.octoprint.logging.LoggingInterceptorLogger
import de.crysxd.octoapp.octoprint.models.connection.ConnectionResponse
import de.crysxd.octoapp.octoprint.models.files.FileObject
import de.crysxd.octoapp.octoprint.models.job.ProgressInformation
import de.crysxd.octoapp.octoprint.models.settings.Settings
import de.crysxd.octoapp.octoprint.models.socket.HistoricTemperatureData
import de.crysxd.octoapp.octoprint.models.socket.Message
import de.crysxd.octoapp.octoprint.plugins.applicationkeys.ApplicationKeysPluginApi
import de.crysxd.octoapp.octoprint.plugins.companion.OctoAppCompanionApi
import de.crysxd.octoapp.octoprint.plugins.companion.OctoAppCompanionApiWrapper
import de.crysxd.octoapp.octoprint.plugins.materialmanager.MaterialManagerPluginsCollection
import de.crysxd.octoapp.octoprint.plugins.mmu2filamentselect.Mmu2FilamentSelectApi
import de.crysxd.octoapp.octoprint.plugins.ngrok.NgrokApi
import de.crysxd.octoapp.octoprint.plugins.octoeverywhere.OctoEverywhereApi
import de.crysxd.octoapp.octoprint.plugins.octolapse.OctolapseApi
import de.crysxd.octoapp.octoprint.plugins.pluginmanager.PluginManagerApi
import de.crysxd.octoapp.octoprint.plugins.power.PowerPluginsCollection
import de.crysxd.octoapp.octoprint.plugins.thespaghettidetective.SpaghettiDetectiveApi
import de.crysxd.octoapp.octoprint.plugins.thespaghettidetective.SpaghettiDetectiveApiWrapper
import de.crysxd.octoapp.octoprint.websocket.ContinuousOnlineCheck
import de.crysxd.octoapp.octoprint.websocket.EventWebSocket
import okhttp3.Cache
import okhttp3.Dns
import okhttp3.EventListener
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.KeyStore
import java.util.concurrent.TimeUnit
import java.util.logging.Level
import java.util.logging.Logger
import javax.net.ssl.HostnameVerifier


class OctoPrint(
    val id: String?,
    private val rawWebUrl: HttpUrl,
    private val rawAlternativeWebUrl: HttpUrl?,
    private val apiKey: String,
    private val highLevelInterceptors: List<Interceptor> = emptyList(),
    private val customDns: Dns? = null,
    private val keyStore: KeyStore? = null,
    private val hostnameVerifier: HostnameVerifier? = null,
    private val httpEventListener: EventListener? = null,
    private val cache: Cache?,
    val readWriteTimeout: Long = 5000,
    val connectTimeoutMs: Long = 10000,
    val webSocketConnectionTimeout: Long = 5000,
    val webSocketPingPongTimeout: Long = 5000,
    private val logging: LoggingConfig,
) {

    val webUrl = rawWebUrl.withoutBasicAuth()
    private val alternativeWebUrl = rawAlternativeWebUrl?.withoutBasicAuth()
    private val alternativeWebUrlInterceptor = AlternativeWebUrlInterceptor(
        logger = createHttpLogger(),
        fullWebUrl = rawWebUrl,
        fullAlternativeWebUrl = rawAlternativeWebUrl
    )
    private val continuousOnlineCheck = ContinuousOnlineCheck(
        url = webUrl,
        localDns = customDns,
        logger = getLogger(),
        onOnline = {
            if (!alternativeWebUrlInterceptor.isPrimaryUsed) {
                getLogger().log(Level.INFO, "Switching back to primary web url")
                alternativeWebUrlInterceptor.isPrimaryUsed = true
                webSocket.reconnect()
            }
        }
    )
    val activeUrl get() = alternativeWebUrlInterceptor.activeUrl
    private val okHttpClient = createOkHttpClient()

    private val webSocket = EventWebSocket(
        httpClient = okHttpClient,
        webUrl = webUrl,
        getCurrentConnectionType = { alternativeWebUrlInterceptor.getActiveConnectionType() },
        gson = createGsonWithTypeAdapters(),
        logger = getLogger(),
        loginApi = createLoginApi(),
        onStart = ::startOnlineCheck,
        onStop = ::stopOnlineCheck,
        pingPongTimeoutMs = webSocketPingPongTimeout,
        connectionTimeoutMs = webSocketConnectionTimeout
    )

    fun performOnlineCheck() {
        continuousOnlineCheck.checkNow()
    }

    private fun startOnlineCheck() {
        continuousOnlineCheck.start()
    }

    private fun stopOnlineCheck() {
        continuousOnlineCheck.stop()
    }

    fun getEventWebSocket() = webSocket

    // For the probe API we use a call timeout to prevent the app from being "stuck".
    // We don't expect large downloads here, so using a call timeout is fine
    suspend fun probeConnection() = createRetrofit().create(ProbeApi::class.java).probe().code()

    fun createUserApi(retrofit: Retrofit = createRetrofit()): UserApi =
        retrofit.create(UserApi::class.java)

    fun createLoginApi(): LoginApi =
        createRetrofit().create(LoginApi::class.java)

    fun createVersionApi(): VersionApi =
        createRetrofit().create(VersionApi::class.java)

    fun createSettingsApi(): SettingsApi =
        createRetrofit().create(SettingsApi::class.java)

    fun createPluginManagerApi(): PluginManagerApi =
        createRetrofit(".").create(PluginManagerApi::class.java)

    fun createPrinterProfileApi(): PrinterProfileApi =
        createRetrofit().create(PrinterProfileApi::class.java)

    fun createJobApi(): JobApi.Wrapper =
        JobApi.Wrapper(createRetrofit().create(JobApi::class.java), webSocket)

    fun createFilesApi(): FilesApi.Wrapper =
        FilesApi.Wrapper(
            webUrl = webUrl,
            okHttpClient = okHttpClient,
            wrapped = createRetrofit().create(FilesApi::class.java)
        )

    fun createPrinterApi(): PrinterApi.Wrapper =
        PrinterApi.Wrapper(createRetrofit().create(PrinterApi::class.java))

    fun createTimelapseApi(): TimelapseApi.Wrapper =
        TimelapseApi.Wrapper(createRetrofit().create(TimelapseApi::class.java))

    fun createPowerPluginsCollection() = PowerPluginsCollection(
        apiRetrofit = createRetrofit(),
        rootRetrofit = createRetrofit(".")
    )

    fun createMaterialManagerPluginsCollection() = MaterialManagerPluginsCollection(createRetrofit("."))

    fun createConnectionApi(): ConnectionApi.Wrapper =
        ConnectionApi.Wrapper((createRetrofit().create(ConnectionApi::class.java)))

    fun createApplicationKeysPluginApi(): ApplicationKeysPluginApi.Wrapper =
        ApplicationKeysPluginApi.Wrapper((createRetrofit(".").create(ApplicationKeysPluginApi::class.java)))

    fun createSystemApi(): SystemApi.Wrapper =
        SystemApi.Wrapper((createRetrofit().create(SystemApi::class.java)))

    fun createOctolapseApi(): OctolapseApi.Wrapper =
        OctolapseApi.Wrapper((createRetrofit(".").create(OctolapseApi::class.java)))

    fun createNgrokApi(): NgrokApi = createRetrofit().create(NgrokApi::class.java)

    fun createOctoEverywhereApi() = createRetrofit().create(OctoEverywhereApi::class.java)

    fun createOctoAppCompanionApi() = OctoAppCompanionApiWrapper(createRetrofit().create(OctoAppCompanionApi::class.java))

    fun createSpaghettiDetectiveApi() = SpaghettiDetectiveApiWrapper(createRetrofit().create(SpaghettiDetectiveApi::class.java))

    fun createMmu2FilamentSelectApi() = Mmu2FilamentSelectApi.Wrapper(createRetrofit().create(Mmu2FilamentSelectApi::class.java))

    fun getLogger(): Logger = OctoPrintLogger

    private fun createHttpLogger(): Logger {
        val logger = Logger.getLogger("OctoPrint/HTTP")
        logger.parent = getLogger()
        logger.useParentHandlers = true
        return logger
    }

    private fun createRetrofit(path: String = "api/", okHttpClient: OkHttpClient = this.okHttpClient) = Retrofit.Builder()
        .baseUrl(webUrl.resolvePath(path))
        .addConverterFactory(GsonConverterFactory.create(createGsonWithTypeAdapters()))
        .client(okHttpClient)
        .build()

    private fun createGsonWithTypeAdapters(): Gson = createBaseGson().newBuilder()
        .registerTypeAdapter(ConnectionResponse.ConnectionState::class.java, ConnectionStateDeserializer(getLogger()))
        .registerTypeAdapter(FileObject::class.java, FileObjectDeserializer(createBaseGson()))
        .registerTypeAdapter(Message::class.java, MessageDeserializer(getLogger(), createBaseGson()))
        .registerTypeAdapter(Settings.PluginSettingsGroup::class.java, PluginSettingsDeserializer())
        .create()

    private fun createBaseGson(): Gson = createRawGson().newBuilder()
        .registerTypeAdapter(HistoricTemperatureData::class.java, HistoricTemperatureDeserializer(createRawGson()))
        .registerTypeAdapter(ProgressInformation::class.java, ProgressInformationDeserializer(createRawGson()))
        .create()

    @Suppress("UNCHECKED_CAST")
    private fun createRawGson(): Gson = GsonBuilder()
        .registerTypeAdapterFactory(object : TypeAdapterFactory {
            override fun <T : Any?> create(gson: Gson?, type: TypeToken<T>?): TypeAdapter<T>? = when (type?.rawType) {
                java.lang.Float::class.java -> SafeFloatAdapter()
                java.lang.Double::class.java -> SafeDoubleAdapter()
                java.lang.Long::class.java -> SafeLongAdapter()
                java.lang.Integer::class.java -> SafeIntAdapter()
                else -> null
            } as TypeAdapter<T>?
        }).create()

    fun createOkHttpClient(): OkHttpClient = OkHttpClient.Builder().apply {
        val logger = createHttpLogger()
        httpEventListener?.let(::eventListener)

        withHostnameVerifier(hostnameVerifier)
        withSslKeystore(keyStore)
        connectTimeout(connectTimeoutMs, TimeUnit.MILLISECONDS)
        readTimeout(readWriteTimeout, TimeUnit.MILLISECONDS)
        writeTimeout(readWriteTimeout, TimeUnit.MILLISECONDS)
        callTimeout((readWriteTimeout * 1.5).toLong(), TimeUnit.MILLISECONDS)
        customDns?.let { dns(it) }
        cache(cache)

        // 1. Add Catch-all interceptor. Uncaught exceptions other than IO lead to a crash,
        // so we wrap any non-IOException in an IOException
        addInterceptor(CatchAllInterceptor(webUrl, apiKey))

        // 2. Add plug-in high level interceptors next
        this@OctoPrint.highLevelInterceptors.forEach { addInterceptor(it) }

        // 3. Sets the API key header
        addInterceptor(ApiKeyInterceptor(apiKey))

        // 4. Consumes raw exceptions and throws wrapped exceptions
        addInterceptor(GenerateExceptionInterceptor {
            createUserApi(createRetrofit(okHttpClient = createOkHttpClient()))
        })

        // 5. This interceptor consumes raw IOException and might switch the host
        addInterceptor(alternativeWebUrlInterceptor)

        // 7. Basic Auth interceptor is the last because we might change the host above
        addInterceptor(BasicAuthInterceptor(logger, rawWebUrl, rawAlternativeWebUrl))

        // 8. Logger needs to be lowest level, we need to log any change made in the stack above
        addInterceptor(
            HttpLoggingInterceptor(LoggingInterceptorLogger(logger)).setLevel(
                when (logging) {
                    LoggingConfig.Production -> HttpLoggingInterceptor.Level.BASIC
                    LoggingConfig.Verbose -> HttpLoggingInterceptor.Level.HEADERS
                    LoggingConfig.Debug -> HttpLoggingInterceptor.Level.BODY
                }
            )
        )
    }.build()
}

//
//val text2 = "{\"current\": {\"state\": {\"text\": \"Printing\", \"flags\": {\"operational\": true, \"printing\": true, \"cancelling\": false, \"pausing\": false, \"resuming\": false, \"finishing\": false, \"closedOrError\": false, \"error\": false, \"paused\": false, \"ready\": false, \"sdReady\": false}, \"error\": \"\"}, \"job\": {\"file\": {\"name\": \"Znackovac_Misha_V3_1h34m_0.2mm_210C_PLA_ENDER3BLTOUCH.aw.gcode\", \"path\": \"Znackovac_Misha_V3_1h34m_0.2mm_210C_PLA_ENDER3BLTOUCH.aw.gcode\", \"display\": \"Znackovac_Misha_V3_1h34m_0.2mm_210C_PLA_ENDER3BLTOUCH.aw.gcode\", \"origin\": \"local\", \"size\": 3515943, \"date\": 1647347839}, \"estimatedPrintTime\": 4922.185297388024, \"averagePrintTime\": 4729.044470541554, \"lastPrintTime\": 4496.47312074, \"filament\": {\"tool0\": {\"length\": 2247.474154417636, \"volume\": 5.405808849061211}}, \"user\": \"\$basicAuthUser\"}, \"currentZ\": 0.4, \"progress\": {\"completion\": 72.36462593392442, \"filepos\": 2544299, \"printTime\": 4344, \"printTimeLeft\": 1284, \"printTimeLeftOrigin\": \"genius\"}, \"offsets\": {}, \"resends\": {\"count\": 0, \"transmitted\": 427663, \"ratio\": 0}, \"serverTime\": 1650553690.8358135, \"temps\": [{\"time\": 1650553690, \"tool0\": {\"actual\": 208.71, \"target\": 210.0}, \"bed\": {\"actual\": 49.74, \"target\": 50.0}, \"chamber\": {\"actual\": null, \"target\": null}, \"Box_Temp\": {\"actual\": \"\", \"target\": null}}], \"logs\": [\"Recv: ok N72014 P2 B3\", \"Send: N72015 G3 X128.431 Y129.664 I7.973 J13.750 E0.17370*89\", \"Recv: ok N72015 P3 B3\", \"Send: N72016 G1 X128.614 Y129.729 E0.18216*98\", \"Recv: ok N72016 P3 B3\", \"Send: N72017 G1 X128.63 Y129.773 E0.18466*91\", \"Recv: ok N72017 P4 B3\", \"Send: N72018 G1 X128.701 Y129.959 E0.19457*98\", \"Recv: ok N72018 P3 B3\", \"Send: N72019 G1 X128.736 Y130.059 E0.19932*104\", \"Recv: ok N72019 P2 B3\", \"Send: N72020 G1 X128.791 Y130.22 E0.20616*80\", \"Recv: ok N72020 P2 B3\", \"Send: N72021 G1 X128.829 Y130.358 E0.21086*109\", \"Recv: ok N72021 P1 B3\", \"Send: N72022 G1 X128.863 Y130.529 E0.21499*106\", \"Recv: ok N72022 P1 B3\", \"Send: N72023 G1 X128.938 Y130.661 E0.21695*101\", \"Recv: ok N72023 P0 B3\", \"Send: N72024 G92 E0*68\", \"Recv: X:128.94 Y:130.66 Z:0.40 E:0.00 Count X:10065 Y:10058 Z:169\", \"Recv: ok N72024 P0 B3\", \"Send: N72025 G1 E-2 F2100*53\", \"Recv: ok N72025 P0 B3\", \"Send: N72026 M204 P1500*80\", \"Recv: ok N72026 P0 B3\", \"Send: N72027 G1 X130.608 Y129.704 F9000*67\", \"Recv: ok N72027 P1 B3\", \"Send: N72028 M204 P500*111\", \"Recv: ok N72028 P1 B3\", \"Send: N72029 G1 X132.278 Y128.748*42\", \"Recv: ok N72029 P0 B3\", \"Send: N72030 G1 E0 F2400*27\", \"Recv: ok N72030 P0 B3\", \"Send: N72031 G1 F934*119\", \"Recv: ok N72031 P1 B3\", \"Send: N72032 G1 X132.213 Y128.709 E0.00065*96\", \"Recv: ok N72032 P1 B3\", \"Send: N72033 G1 X132.224 Y128.539 E0.00369*107\", \"Recv: ok N72033 P0 B3\", \"Send: N72034 G1 X132.22 Y128.357 E0.00876*83\", \"Recv: ok N72034 P0 B3\", \"Send: N72035 G1 X132.201 Y128.16 E0.01606*95\", \"Recv: ok N72035 P0 B3\", \"Send: N72036 G1 X132.188 Y128.061 E0.02032*108\", \"Recv: ok N72036 P0 B3\", \"Send: N72037 G1 X132.168 Y127.949 E0.02567*106\", \"Recv:  T:208.71 /210.00 B:49.74 /50.00 @:48 B@:26\", \"Recv: ok N72037 P1 B3\", \"Send: N72038 G1 X132.135 Y127.79 E0.03393*91\", \"Recv: ok N72038 P1 B3\", \"Send: N72039 G1 X132.162 Y127.644 E0.04246*110\", \"Recv: ok N72039 P0 B3\", \"Send: N72040 G1 X132.214 Y127.472 E0.0556*87\", \"Recv: ok N72040 P0 B3\", \"Send: N72041 G1 X132.423 Y127.377 E0.07332*101\", \"Recv: ok N72041 P0 B3\", \"Send: N72042 M204 P1500*82\", \"Recv: ok N72042 P0 B3\", \"Send: N72043 G1 X132.136 Y127.1 F9000*69\", \"Recv: ok N72043 P1 B3\", \"Send: N72044 M204 P500*101\", \"Recv: ok N72044 P1 B3\", \"Send: N72045 G1 X131.85 Y126.823*31\", \"Recv: ok N72045 P0 B3\", \"Send: N72046 G1 F934*119\", \"Recv: ok N72046 P1 B3\", \"Send: N72047 G1 X131.788 Y126.648 E0.07484*96\", \"Recv: ok N72047 P0 B3\", \"Send: N72048 G1 X131.73 Y126.494 E0.07698*80\", \"Recv: ok N72048 P0 B3\", \"Send: N72049 G1 X131.663 Y126.316 E0.07943*98\"], \"messages\": [\"X:128.94 Y:130.66 Z:0.40 E:0.00 Count X:10065 Y:10058 Z:169\", \"T:208.71 /210.00 B:49.74 /50.00 @:48 B@:26\"], \"busyFiles\": [{\"origin\": \"local\", \"path\": \"Znackovac_Misha_V3_1h34m_0.2mm_210C_PLA_ENDER3BLTOUCH.aw.gcode\"}]}}\n"
//
//val baseGson = GsonBuilder()
//    .registerTypeAdapter(HistoricTemperatureData::class.java, HistoricTemperatureDeserializer())
//    .registerTypeAdapter(ProgressInformation::class.java, ProgressInformationDeserializer(Gson()))
//    .registerTypeAdapterFactory(object : TypeAdapterFactory {
//        override fun <T : Any?> create(gson: Gson?, type: TypeToken<T>?): TypeAdapter<T>? = when (type?.rawType) {
//            java.lang.Float::class.java -> SafeFloatAdapter()
//            java.lang.Double::class.java -> SafeDoubleAdapter()
//            java.lang.Long::class.java -> SafeLongAdapter()
//            java.lang.Integer::class.java -> SafeIntAdapter()
//            else -> null
//        } as TypeAdapter<T>?
//    })
//    .create()
//
//val logger = Logger.getAnonymousLogger()
//val gson = baseGson.newBuilder()
//    .registerTypeAdapter(ConnectionResponse.ConnectionState::class.java, ConnectionStateDeserializer(logger))
//    .registerTypeAdapter(FileObject::class.java, FileObjectDeserializer(baseGson))
//    .registerTypeAdapter(Message::class.java, MessageDeserializer(logger, baseGson))
//    .registerTypeAdapter(Settings.PluginSettingsGroup::class.java, PluginSettingsDeserializer())
//    .create()
//
//try {
//    gson.fromJson(text2, Message::class.java)
//} catch (e: Exception) {
//    e.printStackTrace()
//}
