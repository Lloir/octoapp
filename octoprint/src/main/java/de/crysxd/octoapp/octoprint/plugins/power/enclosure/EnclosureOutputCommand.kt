package de.crysxd.octoapp.octoprint.plugins.power.enclosure

sealed class EnclosureOutputCommand(val status: Boolean) {
    object TurnGpioOn : EnclosureOutputCommand(status = true)
    object TurnGpioOff : EnclosureOutputCommand(status = false)
}

