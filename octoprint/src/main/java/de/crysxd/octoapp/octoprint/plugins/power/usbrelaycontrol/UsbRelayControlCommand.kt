package de.crysxd.octoapp.octoprint.plugins.power.usbrelaycontrol

sealed class UsbRelayControlCommand(val command: String, val id: String?) {
    class TurnUsbRelayOn(device: UsbRelayControlPowerDevice) : UsbRelayControlCommand("turnUSBRelayOn", device.id)
    class TurnUsbRelayOff(device: UsbRelayControlPowerDevice) : UsbRelayControlCommand("turnUSBRelayOff", device.id)
}

