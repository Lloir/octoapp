package de.crysxd.octoapp.octoprint.plugins.octolapse

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class OctolapseSnapshotPlanPreview(
    @SerializedName("preprocessing_job_guid") val jobId: String?,
    @SerializedName("snapshot_plans") val snapshotPlans: SnapshotPlanList? = null
) : Serializable {

    data class SnapshotPlanList(
        @SerializedName("snapshot_plans") val snapshotPlans: List<SnapshotPlan>? = null,
        @SerializedName("printer_volume") val volume: PrinterVolume?,
    ) : Serializable

    data class SnapshotPlan(
        @SerializedName("return_position") val returnPosition: Position? = null,
        @SerializedName("initial_position") val initialPosition: Position? = null,
        @SerializedName("steps") val steps: List<Step>? = null,
    ) : Serializable

    data class Position(
        val x: Float? = null,
        val y: Float? = null,
    ) : Serializable

    data class PrinterVolume(
        @SerializedName("max_x") val width: Float? = null,
        @SerializedName("max_y") val height: Float? = null,
        @SerializedName("origin_type") val origin: Origin? = null,
    ) : Serializable

    enum class Origin : Serializable {
        @SerializedName("lowerleft")
        LowerLeft,

        @SerializedName("center")
        Center
    }

    data class Step(
        val action: String? = null,
        val x: Float? = null,
        val y: Float? = null,
    ) : Serializable
}
