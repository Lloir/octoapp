package de.crysxd.octoapp.notification

import com.google.gson.annotations.SerializedName

data class FcmPrintEvent(
    val fileName: String?,
    val progress: Float?,
    val type: Type?,
    val serverTime: Double?,
    val serverTimePrecise: Double?,
    val timeLeft: Long?,
) {
    enum class Type {
        @SerializedName("printing")
        Printing,

        @SerializedName("paused")
        Paused,

        @SerializedName("completed")
        Completed,

        @SerializedName("filament_required")
        FilamentRequired,

        @SerializedName("idle")
        Idle,

        @SerializedName("mmu_filament_selection_started")
        Mmu2FilamentSelectionStarted,

        @SerializedName("mmu_filament_selection_completed")
        Mmu2FilamentSelectionCompleted,
    }
}

