package de.crysxd.octoapp.base.network

import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import de.crysxd.octoapp.base.BuildConfig
import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.data.models.exceptions.SuppressedIllegalStateException
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.ext.shouldRetry
import de.crysxd.octoapp.base.logging.TimberLogger
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.octoprint.LoggingConfig
import de.crysxd.octoapp.octoprint.OctoPrint
import de.crysxd.octoapp.octoprint.SubjectAlternativeNameCompatVerifier
import de.crysxd.octoapp.octoprint.models.socket.Event
import de.crysxd.octoapp.octoprint.models.socket.Message
import de.crysxd.octoapp.octoprint.websocket.EventFlowConfiguration
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import okhttp3.Cache
import timber.log.Timber
import kotlin.reflect.KClass

@Suppress("EXPERIMENTAL_API_USAGE")
class OctoPrintProvider(
    private val detectBrokenSetupInterceptorFactory: DetectBrokenSetupInterceptor.Factory,
    private val octoPrintRepository: OctoPrintRepository,
    private val octoPreferences: OctoPreferences,
    private val analytics: FirebaseAnalytics,
    private val sslKeyStoreHandler: SslKeyStoreHandler,
    private val localDnsResolver: LocalDnsResolver,
    private val httpCache: Cache,
) {

    private val octoPrintMutex = Mutex()
    private var octoPrintCache = mutableMapOf<String, Pair<String, OctoPrint>>()
    private val cachedMessageFlows = mutableMapOf<String, MutableStateFlow<Map<KClass<out Message>, MessageWrapper<*>>>>()
    private val connectEventFlow = mutableMapOf<String, MutableStateFlow<Event.Connected?>>()
    private val activeInstanceId get() = octoPrintRepository.getActiveInstanceSnapshot()?.id

    init {
        // Passively collect data for the analytics profile
        // The passive event flow does not actively open a connection but piggy-backs other Flows
        AppScope.launch { fillPassiveFlows() }
    }

    private val OctoPrintInstanceInformationV3?.cacheKey get() = this?.apiKey + this?.webUrl + this?.alternativeWebUrl

    fun octoPrintFlow(instanceId: String? = null) = octoPrintRepository.instanceInformationFlow(instanceId).distinctUntilChangedBy {
        it.cacheKey
    }.map { instance ->
        octoPrintMutex.withLock {
            val cached = octoPrintCache[instance?.id]

            when {
                // We don't have information for the instance, clean up
                // Use instanceId not instance.id!
                instance == null -> {
                    Timber.d("Instance is null, clearing cached passive flows")
                    instanceId?.let {
                        createConnectEventFlow(it).tryEmit(null)
                        createCacheMessageFlow(it).tryEmit(emptyMap())
                    }
                    null
                }

                // Use instance.id to ensure handover when active changes!
                cached == null || cached.first != instance.cacheKey -> {
                    val octoPrint = createAdHocOctoPrint(instance, brokenSetupHandlingEnabled = true)
                    Timber.d("Created new OctoPrint: $octoPrint")
                    octoPrintCache[instance.id] = instance.cacheKey to octoPrint
                    octoPrint
                }

                else -> {
                    Timber.d("Took OctoPrint from cache: ${cached.second}")
                    cached.second
                }
            }
        }
    }

    suspend fun octoPrint(instanceId: String? = null): OctoPrint = octoPrintMutex.withLock {
        val id = instanceId ?: octoPrintRepository.getActiveInstanceSnapshot()?.id
        octoPrintCache[id]?.second ?: throw SuppressedIllegalStateException("OctoPrint not available")
    }

    fun getCurrentConnection(instanceId: String?) = (instanceId ?: activeInstanceId)?.let { createConnectEventFlow(it).value }

    fun getLastCurrentMessage(instanceId: String?) = (instanceId ?: activeInstanceId)?.let { createCacheMessageFlow(it).value[Message.CurrentMessage::class]?.message }

    fun passiveConnectionEventFlow(tag: String, instanceId: String? = null) = instanceIdFlow(instanceId)
        .flatMapLatest { it?.let { createConnectEventFlow(it) } ?: emptyFlow() }
        .filterNotNull()
        .onStart { Timber.i("Started connection event flow for $tag on instance $instanceId") }
        .onCompletion { Timber.i("Completed connection event flow for $tag on instance $instanceId") }
        .handleErrors()

    fun passiveCurrentMessageFlow(tag: String, instanceId: String? = null) =
        passiveCachedMessageFlow(tag = tag, clazz = Message.CurrentMessage::class, instanceId = instanceId).filterNotNull()

    @Suppress("UNCHECKED_CAST")
    fun <T : Message> passiveCachedMessageFlow(tag: String, clazz: KClass<T>, instanceId: String? = null) = instanceIdFlow(instanceId)
        .flatMapLatest { it?.let { createCacheMessageFlow(it) } ?: emptyFlow() }
        .distinctUntilChangedBy { it[clazz]?.counter }
        .map { it[clazz]?.message as T? }
        .onStart { Timber.i("Started companion message flow for $tag on instance $instanceId") }
        .onCompletion { Timber.i("Completed companion message flow for $tag on instance $instanceId ") }
        .handleErrors()

    fun eventFlow(tag: String, instanceId: String? = null, config: EventFlowConfiguration = EventFlowConfiguration()) = octoPrintFlow(instanceId)
        .flatMapLatest { it?.getEventWebSocket()?.eventFlow(tag, config) ?: emptyFlow() }
        .catch { e -> Timber.e(e) }
        .onStart { Timber.i("Started event flow for $tag on instance $instanceId") }
        .onCompletion { Timber.i("Completed event message flow for $tag on instance $instanceId ") }
        .handleErrors()

    fun passiveEventFlow(instanceId: String? = null) = octoPrintFlow(instanceId)
        .flatMapLatest { it?.getEventWebSocket()?.passiveEventFlow() ?: emptyFlow() }
        .handleErrors()

    private fun <T> Flow<T>.handleErrors() = retry {
        Timber.e(it)
        delay(1000)
        it.shouldRetry()
    }

    private fun instanceIdFlow(instanceId: String?) =
        (instanceId?.let { flowOf(instanceId) } ?: octoPrintRepository.instanceInformationFlow().map { it?.id }).distinctUntilChanged()

    private fun createConnectEventFlow(instanceId: String) = connectEventFlow.getOrPut(instanceId) { MutableStateFlow(null) }

    private fun createCacheMessageFlow(instanceId: String) = cachedMessageFlows.getOrPut(instanceId) { MutableStateFlow(mutableMapOf()) }

    private fun updateAnalyticsProfileWithEvents(event: Event) {
        when {
            event is Event.MessageReceived && event.message is Message.EventMessage.FirmwareData -> {
                val data = event.message as Message.EventMessage.FirmwareData
                analytics.logEvent("printer_firmware_data") {
                    param("firmware_name", data.firmwareName ?: "unspecified")
                    param("machine_type", data.machineType ?: "unspecified")
                    param("extruder_count", data.extruderCount?.toLong() ?: 0)
                }
                analytics.setUserProperty("printer_firmware_name", data.firmwareName)
                analytics.setUserProperty("printer_machine_type", data.machineType)
                analytics.setUserProperty("printer_extruder_count", data.extruderCount.toString())
            }

            event is Event.MessageReceived && event.message is Message.ConnectedMessage -> {
                OctoAnalytics.logEvent(OctoAnalytics.Event.OctoprintConnected)
                OctoAnalytics.setUserProperty(OctoAnalytics.UserProperty.OctoPrintVersion, (event.message as Message.ConnectedMessage).version)
            }
        }
    }

    private suspend fun fillPassiveFlows() {
        octoPrintRepository.instanceInformationFlow().map {
            // Get all ids, distinct until changed by adding/removing one
            octoPrintRepository.getAll().map { it.id }.sorted()
        }.distinctUntilChanged().flatMapLatest { ids ->
            // Create passive flow for each and combine them
            val flows = ids.map { createFillPassiveFlows(it) }
            combine(flows) { it.toList() }
        }.handleErrors().collect()
    }

    private fun createFillPassiveFlows(instanceId: String) = passiveEventFlow(instanceId).onEach { event ->
        if (instanceId == activeInstanceId) {
            updateAnalyticsProfileWithEvents(event)
        }

        val cacheFlow = createCacheMessageFlow(instanceId)

        (event as? Event.MessageReceived)?.let { messageEvent ->
            // We cache the latest message of any type
            val upgradedMessage = if (messageEvent.message is Message.CurrentMessage) {
                // If the last message had data the new one is lacking, upgrade the new one so the cached message
                // is always holding all information required
                val message = messageEvent.message as Message.CurrentMessage
                val last = cacheFlow.value[Message.CurrentMessage::class]?.message as Message.CurrentMessage?
                message.copy(
                    temps = message.temps.takeIf { it.isNotEmpty() } ?: last?.temps ?: emptyList(),
                    progress = message.progress ?: last?.progress,
                    state = message.state ?: last?.state,
                    job = message.job ?: last?.job,
                )
            } else {
                messageEvent.message
            }

            // We keep an counter in the wrapper so the flow refreshes even when we push the same value twice but still use MutableStateFlow to have `value`
            val value = cacheFlow.value
            val wrapper = MessageWrapper(upgradedMessage, (value[upgradedMessage::class]?.counter ?: -1) + 1)
            cacheFlow.value = value.toMutableMap().apply { put(upgradedMessage::class, wrapper) }
        }

        ((event as? Event.Connected))?.let {
            Timber.i("Connected $instanceId @ ${it.connectionType}")
            createConnectEventFlow(instanceId).value = it
            cacheFlow.value = emptyMap()
        }

        ((event as? Event.Disconnected))?.let {
            Timber.i("Disconnected $instanceId")
            createConnectEventFlow(instanceId).value = null
            cacheFlow.value = emptyMap()
            it.exception?.let(detectBrokenSetupInterceptorFactory.buildFor(instanceId)::handleException)
        }
    }

    fun createAdHocOctoPrint(instance: OctoPrintInstanceInformationV3, brokenSetupHandlingEnabled: Boolean = false) = OctoPrint(
        id = instance.id,
        rawWebUrl = instance.webUrl,
        rawAlternativeWebUrl = instance.alternativeWebUrl,
        apiKey = instance.apiKey,
        highLevelInterceptors = listOfNotNull(
            detectBrokenSetupInterceptorFactory.buildFor(instance.id).takeIf { brokenSetupHandlingEnabled }
        ),
        customDns = localDnsResolver,
        keyStore = sslKeyStoreHandler.loadKeyStore(),
        hostnameVerifier = SubjectAlternativeNameCompatVerifier().takeIf { sslKeyStoreHandler.isWeakVerificationForHost(instance.webUrl) },
        connectTimeoutMs = Firebase.remoteConfig.getLong("connection_timeout_ms"),
        readWriteTimeout = Firebase.remoteConfig.getLong("read_write_timeout_ms"),
        webSocketConnectionTimeout = Firebase.remoteConfig.getLong("web_socket_connect_timeout_ms"),
        webSocketPingPongTimeout = Firebase.remoteConfig.getLong("web_socket_ping_pong_timeout_ms"),
        cache = httpCache,
        logging = when {
            BuildConfig.DEBUG -> LoggingConfig.Debug
            octoPreferences.debugNetworkLogging -> LoggingConfig.Verbose
            else -> LoggingConfig.Production
        },
        httpEventListener = NetworkDebugLogger().takeIf { octoPreferences.debugNetworkLogging }
    ).also { octoPrint ->
        // Setup logger to use timber
        TimberLogger(octoPrint.getLogger())
    }

    private data class MessageWrapper<T : Message>(val message: T, val counter: Int)
}
