package de.crysxd.octoapp.base.utils

import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner

object ExceptionReceivers {

    private val receivers = mutableListOf<(Throwable) -> Unit>()

    fun registerReceiver(lifecycleOwner: LifecycleOwner, receiver: (Throwable) -> Unit) {
        receivers.add(receiver)
        lifecycleOwner.lifecycle.addObserver(object : DefaultLifecycleObserver {
            override fun onDestroy(owner: LifecycleOwner) {
                receivers.remove(receiver)
            }
        })
    }

    fun dispatchException(throwable: Throwable): Boolean {
        receivers.forEach { it(throwable) }
        return receivers.isNotEmpty()
    }
}