@file:Suppress("SpellCheckingInspection")

package de.crysxd.octoapp.octoprint.plugins.pluginmanager

typealias PluginVersion = String
typealias PluginId = String

const val PLUGIN_OCTOAPP_COMPANION: PluginId = "octoapp"
const val PLUGIN_OCTOLAPSE: PluginId = "octolapse"
const val PLUGIN_PSU_CONTROL: PluginId = "psucontrol"
const val PLUGIN_MMU2_FILAMENT_SELECT: PluginId = "mmu2filamentselect"
const val PLUGIN_BETTER_GRBL_SUPPORT: PluginId = "bettergrblsupport"
const val PLUGIN_NGORK: PluginId = "ngrok"
const val PLUGIN_OCTOEVERYWHERE: PluginId = "octoeverywhere"
const val PLUGIN_SPAGHETTI_DETECTIVE: PluginId = "thespaghettidetective"