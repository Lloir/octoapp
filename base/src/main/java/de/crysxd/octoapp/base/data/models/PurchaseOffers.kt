package de.crysxd.octoapp.base.data.models

import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat
import java.util.Locale
import java.util.concurrent.TimeUnit

data class PurchaseOffers(
    val baseConfig: PurchaseConfig,
    val saleConfigs: List<PurchaseConfig>?,
    val purchaseSku: List<String>?,
    val subscriptionSku: List<String>?,
) {

    companion object {
        val DEFAULT = PurchaseOffers(
            baseConfig = PurchaseConfig(
                advertisement = null,
                offers = null,
                validFrom = null,
                validTo = null,
                sellingPoints = listOf(
                    SellingPoint(
                        title = "Gcode preview",
                        description = "See a preview of Gcode files and a live Gcode view during prints",
                        imageUrl = "https://firebasestorage.googleapis.com/v0/b/octoapp-4e438.appspot.com/o/resources%2Fpurchase%2Ffeature_gcode.webp?alt=media&token=e39d0867-00da-4f41-957a-126c246924a8"
                    ),
                    SellingPoint(
                        title = "Multiple printers",
                        description = "Connect multiple printers to OctoApp and switch between them using the Quick Switch menu, home screen widgets, home screen shortcuts and notifications",
                        imageUrl = "https://firebasestorage.googleapis.com/v0/b/octoapp-4e438.appspot.com/o/resources%2Fpurchase%2Ffeature_quick_switch.webp?alt=media&token=152de0c4-6346-4a2b-ada5-2d95cf8272b6"
                    ),
                    SellingPoint(
                        title = "Automatic lights",
                        description = "Let OctoApp automatically turn on and off your lights when you look at your webcam. Supports any light which has 'on' and 'off' controls in OctoApp",
                        imageUrl = "https://firebasestorage.googleapis.com/v0/b/octoapp-4e438.appspot.com/o/resources%2Fpurchase%2Ffeature_auto_lights.webp?alt=media&token=10d451a8-cc4a-41ad-975f-964a3f97fa0a"
                    ),
                    SellingPoint(
                        title = "Infinite app widgets",
                        description = "Create as many widgets and shortcuts on your Android home screen as you want",
                        imageUrl = "https://firebasestorage.googleapis.com/v0/b/octoapp-4e438.appspot.com/o/resources%2Fpurchase%2Ffeature_inifite_widgets.webp?alt=media&token=26bfe707-5f95-4bea-a41e-8317212c0541"
                    ),
                    SellingPoint(
                        title = "Advanced webcam",
                        description = "Make use of the advanced webcam formats RTSP and HLS for higher video quality and lower bandwidth (only works if your OctoPi is configured for those)",
                        imageUrl = "https://firebasestorage.googleapis.com/v0/b/octoapp-4e438.appspot.com/o/resources%2Fpurchase%2Ffeature_advanced_webcam.webp?alt=media&token=33939c08-9c1c-44bd-b2e0-f85827d1159c"
                    ),
                    SellingPoint(
                        title = "File management",
                        description = "Search, upload, copy, rename, download and share files stored on your OctoPrint's storage",
                        imageUrl = "https://firebasestorage.googleapis.com/v0/b/octoapp-4e438.appspot.com/o/resources%2Fpurchase%2Ffeature_files_2.webp?alt=media&token=3424be2f-d5cc-4687-b951-13f61ac5ae7b"
                    ),
                    SellingPoint(
                        title = "File management",
                        description = "Search, upload, copy, rename, download and share files stored on your OctoPrint's storage",
                        imageUrl = "https://firebasestorage.googleapis.com/v0/b/octoapp-4e438.appspot.com/o/resources%2Fpurchase%2Ffeature_files_1.webp?alt=media&token=614688d8-ec6e-42fa-9984-7e5e651703ab"
                    ),
                    SellingPoint(
                        title = "Much more to come!",
                        description = "OctoApp receives a big update every 4-8 weeks with new feature and improvements!",
                        imageUrl = "party"
                    ),
                ),
                texts = Texts(
                    highlightBanner = null,
                    purchaseScreenTitle = "Thank you for<br>supporting OctoApp!",
                    purchaseScreenContinueCta = "Support OctoApp",
                    purchaseScreenDescription = "Hi, I'm Chris! Creating OctoApp and supporting all of you takes a lot of my time. To be able to continue this effort, I decided to make more advanced features exclusive for my supporters.</u><br/><br/><b>I will never start showing ads in this app nor will I take away features that are already available.</b><br/><br/>Following features will be unlocked:",
                    skuListTitle = "High Eight! Thanks for your support! You rock!",
                    launchPurchaseScreenCta = "Support OctoApp",
                    purchaseScreenFeatures = "Multi-OctoPrint<br/>Gcode viewer<br/>HLS webcam streams<br/>Infinite widgets<br/>Automatic lights",
                    purchaseScreenMoreFeatures = "and much more to come!",
                    launchPurchaseScreenHighlight = null,
                )
            ),
            saleConfigs = null,
            purchaseSku = null,
            subscriptionSku = null,
        )
    }

    data class PurchaseConfig(
        val advertisement: Advertisement?,
        val offers: Map<String, Offer>?,
        val texts: Texts,
        val sellingPoints: List<SellingPoint>,
        val validFrom: String?,
        val validTo: String?,
    ) {
        companion object {
            private val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH)
            private const val COUNTDOWN_PLACEHOLDER = "{{countdown}}"
        }

        val validFromDate get() = validFrom?.let(dateFormat::parse)
        val validToDate get() = validTo?.let(dateFormat::parse)

        private val countDown: String
            get() {
                val now = System.currentTimeMillis()
                val until = validToDate?.time ?: Long.MAX_VALUE
                val left = (until - now).coerceAtLeast(0)
                val days = TimeUnit.MILLISECONDS.toDays(left)
                val hours = TimeUnit.MILLISECONDS.toHours(left - TimeUnit.DAYS.toMillis(days))
                val minutes = TimeUnit.MILLISECONDS.toMinutes(left - TimeUnit.DAYS.toMillis(days) - TimeUnit.HOURS.toMillis(hours))
                val seconds = TimeUnit.MILLISECONDS.toSeconds(left - TimeUnit.DAYS.toMillis(days) - TimeUnit.HOURS.toMillis(hours) - TimeUnit.MINUTES.toMillis(minutes))
                return listOfNotNull(
                    String.format("%dd", days).takeIf { days > 0 },
                    String.format("%02dh", hours).takeIf { hours > 0 },
                    String.format("%02dm", minutes),
                    String.format("%02ds", seconds),
                ).joinToString(" ")
            }

        val textsWithData
            get() = texts.copy(
                highlightBanner = texts.highlightBanner?.replace(COUNTDOWN_PLACEHOLDER, countDown),
                launchPurchaseScreenHighlight = texts.launchPurchaseScreenHighlight?.replace(COUNTDOWN_PLACEHOLDER, countDown),
            )
        val advertisementWithData get() = advertisement?.copy(message = advertisement.message.replace(COUNTDOWN_PLACEHOLDER, countDown))
    }

    data class Advertisement(
        val id: String,
        val message: String,
    )

    data class Texts(
        val highlightBanner: String?,
        val purchaseScreenTitle: String,
        val purchaseScreenContinueCta: String,
        val purchaseScreenDescription: String,
        val purchaseScreenFeatures: String,
        val purchaseScreenMoreFeatures: String,
        val skuListTitle: String,
        val launchPurchaseScreenCta: String,
        val launchPurchaseScreenHighlight: String?,
    )

    data class Offer(
        val badge: Badge?,
        val dealFor: String?,
        val label: String?
    )

    data class SellingPoint(
        val title: String?,
        val description: String?,
        val imageUrl: String?,
    )

    enum class Badge {
        @SerializedName("popular")
        Popular,

        @SerializedName("best_value")
        BestValue,

        @SerializedName("sale")
        Sale
    }

    val activeConfig
        get() = saleConfigs?.firstOrNull {
            val from = it.validFromDate
            val to = it.validToDate
            from != null && to != null && System.currentTimeMillis() in from.time..to.time
        } ?: saleConfigs?.firstOrNull {
            val from = it.validFromDate
            val to = it.validToDate
            to == null && from != null && System.currentTimeMillis() > from.time
        } ?: baseConfig
}